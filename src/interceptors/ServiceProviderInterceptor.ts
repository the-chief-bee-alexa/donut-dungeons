import { RequestInterceptor, HandlerInput } from "ask-sdk-core";
import { ServiceProvider } from "../services/ServiceProvider";

export class ServiceProviderInterceptor implements RequestInterceptor {

    private serviceProvider:  ServiceProvider;

    constructor(serviceProvider: ServiceProvider) {
        this.serviceProvider = serviceProvider;
    }

    process(handlerInput: HandlerInput): Promise<void> | void {
        return new Promise((resolve, reject) => {
            let attributes = handlerInput.attributesManager.getRequestAttributes();
            attributes.serviceProvider = this.serviceProvider;
            handlerInput.attributesManager.setRequestAttributes(attributes);
            resolve();
        });
    }
}