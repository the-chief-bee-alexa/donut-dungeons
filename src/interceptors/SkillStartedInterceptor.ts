import { RequestInterceptor, HandlerInput } from "ask-sdk-core";

export class SkillStartedInterceptor implements RequestInterceptor {
    process(handlerInput: HandlerInput): Promise<void> | void {
        return new Promise((resolve, reject) => {
            const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
            if (handlerInput.requestEnvelope.request.type === 'IntentRequest') {
                if (!sessionAttributes.SKILL_STARTED) {
                    return handlerInput.responseBuilder
                    .speak("You must first launch the skill by saying 'open Donut Dungeons'.")
                    .withShouldEndSession(true)
                    .getResponse();
                }
            }

            resolve();
        });
    }
}