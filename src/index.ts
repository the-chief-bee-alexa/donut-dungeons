import { SkillBuilders, DefaultApiClient, Skill } from "ask-sdk-core";
import AWS from "aws-sdk";

import { LaunchRequestHandler } from "./handlers/standard/LaunchRequestHandler";
import { RepeatQuestionHandler } from "./handlers/standard/RepeatQuestionHandler";
import { YesNoIntentHandler } from "./handlers/standard/custom/YesNoIntentHandler";
import { MoveIntentHandler } from "./handlers/standard/MoveIntentHandler";
import { UnlockIntentHandler } from "./handlers/standard/UnlockIntentHandler";
import { PickUpIntentHandler } from "./handlers/standard/PickUpIntentHandler";
import { StartLevelIntentHandler } from "./handlers/standard/StartLevelIntentHandler";
import { CheckCurrentLevelIntentHandler } from "./handlers/standard/CheckCurrentLevelIntentHandler";
import { CheckDonutsLeftIntentHandler } from "./handlers/standard/CheckDonutsLeftIntentHandler";
import { CheckInventoryIntentHandler } from "./handlers/standard/CheckInventoryIntentHandler";
import { LookAroundIntentHandler } from "./handlers/standard/LookAroundIntentHandler";
import { HelpIntentHandler } from "./handlers/standard/HelpIntentHandler";
import { WhatCanIBuyIntentHandler } from "./handlers/in_skill_purchasing/WhatCanIBuyIntentHandler";
import { BuyOrUpsellResponseHandler } from "./handlers/in_skill_purchasing/BuyOrUpsellResponseHandler";
import { RefundIntentHandler } from "./handlers/in_skill_purchasing/RefundIntentHandler";
import { RefundReponseHandler } from "./handlers/in_skill_purchasing/RefundResponseHandler";
import { BuyPremPackIntentHandler } from "./handlers/in_skill_purchasing/BuyPremPackIntentHandler";
import { CancelIntentHandler } from "./handlers/standard/CancelIntentHandler";
import { StopIntentHandler } from "./handlers/standard/StopIntentHandler";
import { FallbackIntentHandler } from "./handlers/standard/FallbackIntentHandler";
import { SessionEndedRequestHandler } from "./handlers/standard/SessionEndedRequestHandler";

import { ServiceProviderInterceptor } from "./interceptors/ServiceProviderInterceptor";
import { ServiceProvider } from "./services/ServiceProvider";
import { DatabaseProvider } from "./db/DatabaseProvider";
import { SkillStartedInterceptor } from "./interceptors/SkillStartedInterceptor";

AWS.config.update({ region: 'us-east-1' });

let skill: Skill;
let dbClient: AWS.DynamoDB.DocumentClient;
let dbProvider: DatabaseProvider;
let serviceProvider: ServiceProvider;

export const handler = async (event: any, context: any) => {
    initialiseProviders();

    if (!skill) {
        skill = SkillBuilders.custom()
            .addRequestHandlers(
                new LaunchRequestHandler(),
                new RepeatQuestionHandler(),
                new YesNoIntentHandler(),
                new MoveIntentHandler(),
                new PickUpIntentHandler(),
                new UnlockIntentHandler(),
                new StartLevelIntentHandler(),
                new CheckCurrentLevelIntentHandler(),
                new CheckDonutsLeftIntentHandler(),
                new CheckInventoryIntentHandler(),
                new LookAroundIntentHandler(),
                new HelpIntentHandler(),
                new WhatCanIBuyIntentHandler(),
                new BuyOrUpsellResponseHandler(),
                new RefundIntentHandler(),
                new RefundReponseHandler(),
                new BuyPremPackIntentHandler(),
                new CancelIntentHandler(),
                new StopIntentHandler(),
                new FallbackIntentHandler(),
                new SessionEndedRequestHandler()
            )
            .addRequestInterceptors(new SkillStartedInterceptor())
            .addRequestInterceptors(new ServiceProviderInterceptor(serviceProvider))
            .withApiClient(new DefaultApiClient())
            .create();
    }

    return await skill.invoke(event, context);
};

const initialiseProviders = () => {
    if (!dbClient) {
        let clientOptions = undefined;
        if (process.env.STAGE === "local") {
            clientOptions = {
                region: "localhost",
                endpoint: "http://localhost:8000"
            };
        }
        
        dbClient = new AWS.DynamoDB.DocumentClient(clientOptions);
        const usersTableName = process.env.DYNAMO_DB_USERS_TABLE;
        if (!usersTableName) {
            throw new Error("Could not retrieve usersTableName.");
        }

        dbProvider = new DatabaseProvider(dbClient, usersTableName);
        serviceProvider = new ServiceProvider(dbProvider);
    }
}
