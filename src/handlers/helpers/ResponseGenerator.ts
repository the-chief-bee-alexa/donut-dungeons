import { Donut } from "../../objects/items/Donut";
import { GameObject } from "../../objects/GameObject";
import { Key } from "../../objects/items/Key";
import { HIGHEST_LEVEL } from "../standard/StartLevelIntentHandler";

export class ResponseGenerator {
    public static generateDonutsLeftToFindResponse(donutsLeftToFind: Array<GameObject>): string {
        let response = "You need to find ";
        const donutTypesCount = donutsLeftToFind.length;

        if (donutsLeftToFind.length === 0) {
            throw new Error("Must have at least 1 donut to call generateDonutsLeftToFindResponse.");
        }

        if (donutTypesCount > 1) {
            for (let i = 0; i < donutTypesCount - 1; i++) {
                const donut = donutsLeftToFind[i] as Donut;
                const s = donut.getQuantity() > 1 ? 's' : '';
                response = response + donut.getQuantity() + ' ' + donut.getName() + s + ', ';
            }

            response = response + "and ";
        }

        const lastDonutType = donutsLeftToFind[donutTypesCount-1];
        const lastDonut = lastDonutType as Donut;
        const s = lastDonut.getQuantity() > 1 ? 's' : '';
       
        return response + lastDonut.getQuantity() + " " + lastDonut.getName() + s + ". What will you do next?";
    }

    public static generateLevelCompletedResponse(currentLevel: number): string {
        let message = '';
        if (currentLevel < HIGHEST_LEVEL) {
            message  = `Would you like to start the next level?`;
        } else {
            message = `Great job! You have completed all the levels of Donut Dungeons. You can replay any level, 
            for instance, you could say 'start level one'. What would you like to do?`;
        }

        return `Congratulations! You have found all the missing donuts in this dungeon. 
        Level ${currentLevel} is now complete. ${message}`;
    }

    public static generateInventoryResponse(inventory: Array<GameObject>): string {
        const invLength = inventory.length;
        if (invLength === 0) {
            return 'There are no items in your inventory. What will you do next?';
        }

        let response = "Your inventory contains ";
        if (invLength > 1) {
            for (let i = 0; i < invLength - 1; i++) {
                const secondLastItem = invLength - 2;
                const comma = i === secondLastItem ? "" : ",";
                response = this.appendItemDescription(response, inventory[i]) + comma;
            }
            response = response + " and ";
        }

        const lastItem = inventory[invLength-1];
        response = this.appendItemDescription(response, lastItem);
       
        return response + ". What will you do next?";
    }

    private static appendItemDescription(response: string, item: GameObject): string {
        if (item instanceof Donut) {
            const qty = item.getQuantity();
            const name = item.getName();
            const s = qty > 1 ? 's' : '';
            return response + `${qty} ${name}${s}`;
        }
        
        if (item instanceof Key) {
            return response + `a ${item.getName()}`;
        }

        throw new Error("Could not generate inventory response for items.");
    }
}