import { HandlerInput, RequestHandler } from "ask-sdk-core";
import { Response, IntentRequest } from "ask-sdk-model";

import { ServiceProvider } from "../services/ServiceProvider";
import { AbstractService } from "../services/AbstractService";
import { ServiceEnum } from "../services/ServiceEnum";
import { UserService } from "../services/implementations/UserService";
import { User } from "../objects/general/User";

export abstract class AbstractRequestHandler implements RequestHandler {

    private handlerInput: HandlerInput | null = null;
    abstract canHandle(handlerInput: HandlerInput): boolean;
    abstract async handle(handlerInput: HandlerInput): Promise<Response>;

    protected setHandlerInput(handlerInput: HandlerInput): void {
        this.handlerInput = handlerInput;
    }

    protected getHandlerInput(): HandlerInput {
        if (this.handlerInput) {
            return this.handlerInput;
        }
       
        throw new Error("handlerInput is null!");
    }

    protected addSessionAttribute(key: string, value: string): void {
        if (!this.handlerInput) {
            throw new Error("handlerInput is null!");
        }

        const attributesManager = this.handlerInput.attributesManager;

        let sessionAttributes = attributesManager.getSessionAttributes();
        sessionAttributes[key] = value;

        attributesManager.setSessionAttributes(sessionAttributes);
    }

    protected removeSessionAttribute(key: string): void {
        if (!this.handlerInput) {
            throw new Error("handlerInput is null!");
        }

        const attributesManager = this.handlerInput.attributesManager;

        let sessionAttributes = attributesManager.getSessionAttributes();
        delete sessionAttributes[key];

        attributesManager.setSessionAttributes(sessionAttributes);
    }

    protected getSessionAttributes(): any {
        if (!this.handlerInput) {
            throw new Error("handlerInput is null!");
        }

        return this.handlerInput.attributesManager.getSessionAttributes();
    }

    protected getSlotValue(slotName: string): any {
        if (!this.handlerInput) {
            throw new Error("handlerInput is null!");
        }

        const request = this.handlerInput.requestEnvelope.request as IntentRequest;
        const slots = request.intent.slots;

        if (slots) {
            return slots[slotName].value;
        }

        throw new Error("There is no such Slot called " + slotName);
    }

    protected getService(serviceName: ServiceEnum): AbstractService {
        if (!this.handlerInput) {
            throw new Error("handlerInput is null!");
        }

        const reqAttributes = this.handlerInput.attributesManager.getRequestAttributes();
        const serviceProvider: ServiceProvider = reqAttributes.serviceProvider;
        return serviceProvider.getService(serviceName);
    }

    protected getUserId(): string {
        if (!this.handlerInput) {
            throw new Error("handlerInput is null!");
        }

        const session = this.handlerInput.requestEnvelope.session;
        if (session) {
            return session.user.userId;
        }

        throw new Error("Could not get 'handlerInput.requestEnvelope.session.user.userId'");
    }

    protected async getUser(): Promise<User | void> {
        const userService = this.getService(ServiceEnum.UserService) as UserService;
        const userId = this.getUserId();
        return userService.getUserById(userId);
    }
};