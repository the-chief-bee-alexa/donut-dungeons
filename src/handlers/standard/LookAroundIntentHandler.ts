import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { ServiceEnum } from "../../services/ServiceEnum";
import { UserService } from "../../services/implementations/UserService";
import { CellService } from "../../services/implementations/CellService";

export class LookAroundIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'LookAroundIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        const userId = this.getUserId();

        const userService = this.getService(ServiceEnum.UserService) as UserService;
        const userAtts = await userService.getUserAttributes(userId, ["currentLocation"]);

        if (userAtts && userAtts.currentLocation) {
            const cellService = this.getService(ServiceEnum.CellService) as CellService;
            const cell =  await cellService.getCell(userId, userAtts.currentLocation);

            if (!cell) {
                throw new Error("Cell could not be identified when executing LookAroundIntentHandler.");
            }

            const cellDescription = cell.getDescription() !== "" ? cell.getDescription() : "There is nothing here.";
            let speechText = `${cellDescription} What will you do?`;

            return this.getHandlerInput().responseBuilder
            .speak(speechText)
            .withShouldEndSession(false)
            .getResponse();
        }

        throw new Error("An error occurred when calling CheckDonutsLeftIntentHandler.");
    }
};