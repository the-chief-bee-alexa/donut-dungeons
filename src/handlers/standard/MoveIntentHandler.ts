import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { ServiceEnum } from "../../services/ServiceEnum";
import { UserActionService } from "../../services/implementations/UserActionService";
import { GameObjectEnum } from "../../objects/GameObjectEnum";
import { DirectionEnum } from "../../objects/general_enums/DirectionEnum";
import { Cell } from "../../objects/structures/Cell";
import { GameObject } from "../../objects/GameObject";

export class MoveIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'MoveIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        const userId = this.getUserId();
        const intendedMoveDirection = this.getSlotValue("Direction");

        const slotValidation = this.validateDirectionSlot(intendedMoveDirection);
        if (slotValidation) {
            return slotValidation;
        }

        const uaService = this.getService(ServiceEnum.UserActionService) as UserActionService;
        const moveResult: [boolean, Cell | void, GameObject] = await uaService.move(userId, intendedMoveDirection);
       
        const speechText = this.getResponse(moveResult, intendedMoveDirection);

        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .withShouldEndSession(false)
        .getResponse();
    }

    private validateDirectionSlot(intendedMoveDirection: DirectionEnum): Response | void {
        const isValidDirection = Object.values(DirectionEnum).includes(intendedMoveDirection);
        if (!isValidDirection) {
            return this.getHandlerInput().responseBuilder
            .speak("You can only move left, right, up or down. Please try again.")
            .withShouldEndSession(false)
            .getResponse();
        }
    }

    private getResponse(moveResult: [boolean, Cell | void, GameObject], direction: DirectionEnum): string {
        const moved = moveResult["0"];
        const newCell = moveResult["1"];
        const gameObjectInDirection = moveResult["2"];

        const objectType = gameObjectInDirection.getType();
        const objectName = gameObjectInDirection.getName();
        let response = "";

        if (!moved) {
            const locked = objectType === GameObjectEnum.DOOR ? "locked " : "";
            return response = "There is a " + locked + objectName + " blocking your way. Where to next?";
        }

        if (!newCell) {
            throw new Error("Could not retrieve new cell user has moved to.");
        }

        switch (objectType) {
            case GameObjectEnum.LADDER:
                response = "You climb " + direction + " the ladder. ";
                break;
            case GameObjectEnum.CORRIDOR:
                response = "You move " + direction + " along the corridor. "
                break;
            case GameObjectEnum.DOOR:
                response = "You move " + direction + " through the open door. ";
                break;
        }

        response = response + newCell.getDescription();

        if (response === "") {
            throw new Error("Could not recognise move result when User tried to move.");
        }

        response = response + " Where to next?";
    
        return response;
    }
};