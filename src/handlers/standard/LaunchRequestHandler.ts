import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { UserService } from "../../services/implementations/UserService";

import { QuestionEnum } from "./custom/YesNoIntentHandler";
import { GameStateEnum } from "../../objects/general_enums/GameStateEnum";
import { ServiceEnum } from "../../services/ServiceEnum";

import { UserAttributes } from "../../objects/general/UserAttributes";
import { User } from "../../objects/general/User";
import { HIGHEST_LEVEL } from "./StartLevelIntentHandler";
import { GameObject } from "../../objects/GameObject";
import { GameObjectFactory } from "../../objects/GameObjectFactory";
import { ResponseGenerator } from "../helpers/ResponseGenerator";
import { DonutService } from "../../services/implementations/DonutService";

const WELCOME_NEW_PLAYER = `Welcome to the world of Donut Dungeons! Here you will explore 
many dungeons, large and small, filled with hidden donuts, waiting to be found.
Your mission is to explore these dungeons and find the donuts. It won't be easy though,
you will need to overcome many obstacles and puzzles that will come your way, to reach the 
tasty goodness. Now, are you ready to begin the first dungeon? Say yes or no.`; 
const WELCOME_NEW_PLAYER_REPROMPT = `Are you ready to begin the first dungeon? Say yes or no.`;

export class LaunchRequestHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;
        return request.type === "LaunchRequest";
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);
        this.addSessionAttribute("SKILL_STARTED", "SKILL_STARTED");

        const user = await this.getUser();
        const response = !user ? await this.handleNewUser() : await this.handleExistingUser(user);

        const speechText = response["0"];
        const repromptText = response["1"];

        return this.getHandlerInput().responseBuilder
            .speak(speechText)
            .reprompt(repromptText)
            .getResponse();
    }

    private async handleNewUser(): Promise<[string, string]> {
        const speechText = WELCOME_NEW_PLAYER;
        const repromptText = WELCOME_NEW_PLAYER_REPROMPT;
        this.addSessionAttribute("question", QuestionEnum.BeginFirstPlay);

        return [speechText, repromptText];
    }

    private async handleExistingUser(user: User): Promise<[string, string]> {
        const userService = this.getService(ServiceEnum.UserService) as UserService;
        const userAtts: UserAttributes | void = await userService.getUserAttributes(
            user.getUserId(), 
            ["gameState", "currentLevel", "inventory"]
        );

        if (userAtts && userAtts.gameState && userAtts.currentLevel) {
            const currentLevel = userAtts.currentLevel;
            let speechText, reprompt = null;
            switch (userAtts.gameState) {
                case GameStateEnum.IN_PROGRESS:
                    const inventory = this.jsonInventoryToGameObjectArray(userAtts.inventory);
                    const donutsLeftDescription = this.getDonutsLeftToFindResponse(inventory, currentLevel);
                    
                    speechText = `Welcome back! You are currently in dungeon level ${currentLevel}. Time to 
                    find those donuts! ${donutsLeftDescription}`;
                    break;
                case GameStateEnum.COMPLETE:
                    if (currentLevel < HIGHEST_LEVEL) {
                        this.addSessionAttribute("question", QuestionEnum.StartNextLevel);
                        speechText = `Welcome back! In the last game, you successfully completed dungeon level ${currentLevel}. Would 
                        you like to start the next level?`;
                    } else {
                        speechText = `Welcome back! In the last game you completed the final level of Donut Dungeons. You can replay any of the levels, 
                        for instance, you could say 'start level one'. What would you like to do?`;
                    }
                    break;
                default:
                    throw new Error("Could not retrieve Game State when handling existing user");
            }

            await userService.recordLogin(user.getUserId(), new Date().getTime());
            reprompt = speechText;
            return [speechText, reprompt];
        }

        throw new Error("Could not handle existing user when launching skill.");
    }

    private getDonutsLeftToFindResponse(inventory: Array<GameObject>, currentLevel: number): string {
        const donutService = this.getService(ServiceEnum.DonutService) as DonutService;
        const donutsLeftToFind = donutService.calculateDonutsLeftToFind(currentLevel, inventory);
        return ResponseGenerator.generateDonutsLeftToFindResponse(donutsLeftToFind);
    }

    private jsonInventoryToGameObjectArray(jsonInventory: any): Array<GameObject> {
        const gameObjectFactory = new GameObjectFactory();
        return jsonInventory.map((itemJson: any) => {
            return gameObjectFactory.createItemFromJson(itemJson);
        });
    }
}