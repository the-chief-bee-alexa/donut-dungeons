import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { ServiceEnum } from "../../services/ServiceEnum";
import { GameObjectEnum } from "../../objects/GameObjectEnum";
import { UserActionService } from "../../services/implementations/UserActionService";
import { GameObject } from "../../objects/GameObject";
import { DonutService } from "../../services/implementations/DonutService";
import { LevelCompletedEvent } from "../../objects/events/implementations/LevelCompletedEvent";
import { ResponseGenerator } from "../helpers/ResponseGenerator";
import { QuestionEnum } from "./custom/YesNoIntentHandler";
import { PickUpSuccessEvent } from "../../objects/events/implementations/PickUpSuccessEvent";
import { PickUpFailEvent } from "../../objects/events/implementations/PickUpFailEvent";
import { PickUpFailReasonEnum } from "../../objects/general_enums/PickUpFailReasonEnum";
import { HIGHEST_LEVEL } from "./StartLevelIntentHandler";

export class PickUpIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'PickUpIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        const userId = this.getUserId();
        let itemToPick = this.getSlotValue("Item");
        // TODO: Fix nasty hack
        if (itemToPick === 'doughnut') {
            itemToPick = 'donut';
        }
        
        const slotValidation = this.validateItemSlot(itemToPick);
        if (slotValidation) {
            return slotValidation;
        }
        
        const uaService = this.getService(ServiceEnum.UserActionService) as UserActionService;
        const res = await uaService.pickUpItem(userId, itemToPick.toUpperCase() as GameObjectEnum);

        const speechText = this.getResponse(res);

        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .withShouldEndSession(false)
        .getResponse();
    }

    private validateItemSlot(item: string): Response | void {
        if (!item || (item !== "donut" && item !== "key")) {
            return this.getHandlerInput().responseBuilder
            .speak("You can only pick up a key or a donut. Please try again.")
            .withShouldEndSession(false)
            .getResponse();
        }
    }

    private getResponse(event: PickUpSuccessEvent | PickUpFailEvent | LevelCompletedEvent): string {
        const next = " What next?";
        if (event instanceof PickUpSuccessEvent) {
            let response = "";
            const pickedUpItem = event.getPickedUpItem();
            const updatedInventory = event.getUpdatedInventory();
            const currentLevel = event.getCurrentLevel();
            response = this.getPickUpResponse(pickedUpItem);

            if (pickedUpItem.getType() === GameObjectEnum.DONUT) {
                const donutsLeftToFind = this.getDonutsLeftToFindResponse(updatedInventory, currentLevel);
                response = response + " " + donutsLeftToFind;
            } else {
                response = response + next;
            }

            return response;
        }

        if (event instanceof PickUpFailEvent) {
            switch (event.getReason()) {
                case PickUpFailReasonEnum.NO_SUCH_ITEM:
                    return "There is no such item to pick up here. What next?";
                default:
                    throw new Error("Was unable to generate a response for UnlockFailEvent.");
            }
        }

        if (event instanceof LevelCompletedEvent) {
            if (event.getCompletedLevel() < HIGHEST_LEVEL) {
                this.addSessionAttribute("question", QuestionEnum.StartNextLevel);
            }
          
            const lastDonut = event.getOptionalData().lastDonut;
            if (!lastDonut) {
                throw new Error("LevelCompletedEvent did not have a last donut when executing the pick up.");
            }

            return this.getPickUpResponse(lastDonut) + " " + this.getLevelCompletedResponse(event);
        }
        
        throw new Error("Was unable to generate a response for picking up item.");
    }

    private getPickUpResponse(pickedUpItem: GameObject): string {
        return `You pick up the ${pickedUpItem.getName()} and add it to your inventory.`;
    }

    private getLevelCompletedResponse(event: LevelCompletedEvent): string {
        const currentLevel = event.getCompletedLevel();
        return ResponseGenerator.generateLevelCompletedResponse(currentLevel);
    }

    private getDonutsLeftToFindResponse(inventory: Array<GameObject>, currentLevel: number): string {
        const donutService = this.getService(ServiceEnum.DonutService) as DonutService;
        const donutsLeftToFind = donutService.calculateDonutsLeftToFind(currentLevel, inventory);
        return ResponseGenerator.generateDonutsLeftToFindResponse(donutsLeftToFind);
    }
};