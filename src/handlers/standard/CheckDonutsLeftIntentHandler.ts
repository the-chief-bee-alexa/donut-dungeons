import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { ServiceEnum } from "../../services/ServiceEnum";
import { UserService } from "../../services/implementations/UserService";
import { GameObject } from "../../objects/GameObject";
import { DonutService } from "../../services/implementations/DonutService";
import { ResponseGenerator } from "../helpers/ResponseGenerator";
import { GameObjectFactory } from "../../objects/GameObjectFactory";

export class CheckDonutsLeftIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'CheckDonutsLeftIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        const userId = this.getUserId();

        const userService = this.getService(ServiceEnum.UserService) as UserService;
        const userAtts = await userService.getUserAttributes(userId, ["currentLevel", "inventory"]);

        if (userAtts && userAtts.currentLevel && userAtts.inventory) {
            const inventory = this.jsonInventoryToGameObjectArray(userAtts.inventory);
            let speechText = this.getDonutsLeftToFindResponse(inventory, userAtts.currentLevel);
            return this.getHandlerInput().responseBuilder
            .speak(speechText)
            .withShouldEndSession(false)
            .getResponse();
        }

        throw new Error("An error occurred when calling CheckDonutsLeftIntentHandler.");
    }

    private getDonutsLeftToFindResponse(inventory: Array<GameObject>, currentLevel: number): string {
        const donutService = this.getService(ServiceEnum.DonutService) as DonutService;
        const donutsLeftToFind = donutService.calculateDonutsLeftToFind(currentLevel, inventory);
        return ResponseGenerator.generateDonutsLeftToFindResponse(donutsLeftToFind);
    }

     private jsonInventoryToGameObjectArray(jsonInventory: any): Array<GameObject> {
        const gameObjectFactory = new GameObjectFactory();
        return jsonInventory.map((itemJson: any) => {
            return gameObjectFactory.createItemFromJson(itemJson);
        });
    }
};