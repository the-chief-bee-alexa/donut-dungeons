import { HandlerInput } from "ask-sdk-core";
import { Response, IntentRequest } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { ServiceEnum } from "../../services/ServiceEnum";
import { UserService } from "../../services/implementations/UserService";
import { GameObject } from "../../objects/GameObject";
import { DonutService } from "../../services/implementations/DonutService";
import { ResponseGenerator } from "../helpers/ResponseGenerator";
import { GameObjectFactory } from "../../objects/GameObjectFactory";
import { InitialiseLevelSuccessEvent } from "../../objects/events/implementations/InitialiseLevelSuccessEvent";
import { InitialiseLevelFailEvent } from "../../objects/events/implementations/InitialiseLevelFailEvent";
import { InitialiseLevelFailReasonEnum } from "../../objects/general_enums/InitialiseLevelFailReasonEnum";
import { QuestionEnum } from "./custom/YesNoIntentHandler";

export const HIGHEST_LEVEL = 10;
export class StartLevelIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'StartLevelIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        const userId = this.getUserId();
        const level: string = this.getSlotValue("Level");
        const levelToStart = Number(level);

        const request: IntentRequest = this.getHandlerInput().requestEnvelope.request as IntentRequest;
        const intent = request.intent;

        let speechText = '';

        if(intent.confirmationStatus === 'CONFIRMED') {
            if (levelToStart > HIGHEST_LEVEL) {
                speechText = `Sorry, the highest possible level is currently ${HIGHEST_LEVEL}. What will you do next?`;
            } else {
                const locale = this.getHandlerInput().requestEnvelope.request.locale;
                const serviceClientFactory = this.getHandlerInput().serviceClientFactory;

                const uaService = this.getService(ServiceEnum.UserService) as UserService;
                const event = await uaService.initialiseLevel(userId, Number(levelToStart), serviceClientFactory, locale);

                if (event instanceof InitialiseLevelSuccessEvent) {
                    speechText = await this.getInitialiseLevelSuccessResponse(userId, levelToStart);
                }

                if (event instanceof InitialiseLevelFailEvent) {
                    return this.getInitialiseLevelFailResponse(event, levelToStart);
                }
            }
        }

        if(intent.confirmationStatus === 'DENIED') {
            speechText = 'Alright. You are still in the current dungeon. What would you like to do?';
        }
        
        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .withShouldEndSession(false)
        .getResponse();
    }

    private async getInitialiseLevelSuccessResponse(userId: string, level: number): Promise<string> {
        const userService = this.getService(ServiceEnum.UserService) as UserService;
        const userAtts = await userService.getUserAttributes(userId, ["currentLevel", "inventory"]);
        if (userAtts && userAtts.currentLevel && userAtts.inventory) {
            const inventory = this.jsonInventoryToGameObjectArray(userAtts.inventory);
            const donutsLeftDesc = this.getDonutsLeftToFindResponse(inventory, userAtts.currentLevel);

            return `Alright, a new game has started and you have been dropped into 
            dungeon level ${level}. ${donutsLeftDesc}`;
        }

        throw new Error("Unable to retrieve user data when forming response for StartLevelIntentHandler.");
    }

    private async getInitialiseLevelFailResponse(event: InitialiseLevelFailEvent, level: number): Promise<Response> {
        const reason = event.getReason();

        switch (reason) {
            case InitialiseLevelFailReasonEnum.LEVEL_NOT_REACHED:
                const levelNotReachedText = `You must have completed all levels prior to level ${level} before you
                can begin this level. What would you like to do next?`;

                return this.getHandlerInput().responseBuilder
                .speak(levelNotReachedText)
                .withShouldEndSession(false)
                .getResponse();
            case InitialiseLevelFailReasonEnum.USER_NOT_ENTITLED:
                this.addSessionAttribute("question", QuestionEnum.BuyPremiumLevelsPackage);
                return this.getHandlerInput().responseBuilder
                .speak(`This level requires Premium Levels Package to play. This package allows you to access all 
                free and paid levels. Would you like to buy this?`)
                .getResponse();
        }

        throw new Error("Could not find a valid InitialiseLevelFailEvent reason when creating InitialiseLevelFail response.");
    }

    private getDonutsLeftToFindResponse(inventory: Array<GameObject>, currentLevel: number): string {
        const donutService = this.getService(ServiceEnum.DonutService) as DonutService;
        const donutsLeftToFind = donutService.calculateDonutsLeftToFind(currentLevel, inventory);
        return ResponseGenerator.generateDonutsLeftToFindResponse(donutsLeftToFind);
    }

    private jsonInventoryToGameObjectArray(jsonInventory: any): Array<GameObject> {
        const gameObjectFactory = new GameObjectFactory();
        return jsonInventory.map((itemJson: any) => {
            return gameObjectFactory.createItemFromJson(itemJson);
        });
    }
};