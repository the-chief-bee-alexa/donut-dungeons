import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { ServiceEnum } from "../../services/ServiceEnum";
import { UserService } from "../../services/implementations/UserService";

export class CheckCurrentLevelIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'CheckCurrentLevelIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        const userId = this.getUserId();

        const userService = this.getService(ServiceEnum.UserService) as UserService;
        const userAtts = await userService.getUserAttributes(userId, ["currentLevel"]);

        if (userAtts && userAtts.currentLevel) {
            let speechText = this.getResponse(userAtts.currentLevel);
            return this.getHandlerInput().responseBuilder
            .speak(speechText)
            .withShouldEndSession(false)
            .getResponse();
        }

        throw new Error("An error occurred when calling CheckCurrentLevelIntentHandler.");
    }

    private getResponse(currentLevel: number): string {
        return `You are currently at dungeon level ${currentLevel}. What will you do?`;
    }
};