import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { ResponseGenerator } from "../helpers/ResponseGenerator";
import { UserActionService, UnlockUserActionEvent } from "../../services/implementations/UserActionService";
import { DonutService } from "../../services/implementations/DonutService";

import { ServiceEnum } from "../../services/ServiceEnum";
import { GameObjectEnum } from "../../objects/GameObjectEnum";
import { UnlockFailReasonEnum } from "../../objects/general_enums/UnlockFailReasonEnum";
import { QuestionEnum } from "./custom/YesNoIntentHandler";

import { GameObject } from "../../objects/GameObject";
import { Chest } from "../../objects/items/Chest";
import { Door } from "../../objects/structures/Door";

import { UnlockSuccessEvent } from "../../objects/events/implementations/UnlockSuccessEvent";
import { UnlockFailEvent } from "../../objects/events/implementations/UnlockFailEvent";
import { LevelCompletedEvent } from "../../objects/events/implementations/LevelCompletedEvent";
import { HIGHEST_LEVEL } from "./StartLevelIntentHandler";


export class UnlockIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'UnlockIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        const userId = this.getUserId();
        let itemToUnlock = this.getSlotValue("Item");
        const slotValidation = this.validateItemSlot(itemToUnlock);
        if (slotValidation) {
            return slotValidation;
        }
        
        const uaService = this.getService(ServiceEnum.UserActionService) as UserActionService;
        const event = await uaService.unlock(userId, itemToUnlock.toUpperCase() as GameObjectEnum);
        const speechText = this.getResponse(event);

        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .withShouldEndSession(false)
        .getResponse();
    }

    private validateItemSlot(item: string): Response | void {
        if (!item || (item !== "door" && item !== "chest")) {
            return this.getHandlerInput().responseBuilder
            .speak("You can only unlock a locked chest or a locked door. Please try again.")
            .withShouldEndSession(false)
            .getResponse();
        }
    }

    private getResponse(event: UnlockUserActionEvent): string {
        const next = " What next?";
        if (event instanceof UnlockSuccessEvent) {
            return this.getUnlockSuccessResponse(event) + next;
        }

        if (event instanceof UnlockFailEvent) {
            return this.getUnlockFailResponse(event) + next;
        }

        if (event instanceof LevelCompletedEvent) {
            if (event.getCompletedLevel() < HIGHEST_LEVEL) {
                this.addSessionAttribute("question", QuestionEnum.StartNextLevel);
            }

            const chest = event.getOptionalData().openedChest;
            if (!chest) {
                throw new Error("LevelCompletedEvent did not have a chest object when unlocking.");
            }
            return this.getUnlockChestResponse(chest) + " " + this.getLevelCompletedResponse(event);
        }

        throw new Error("Could not generate response when unlocking item.");
    }

    private getUnlockSuccessResponse(event: UnlockSuccessEvent): string {
        const unlockedItem = event.getUnlockedItem();
        const updatedInventory = event.getUpdatedInventory();

        if (unlockedItem instanceof Chest) {
            let response =  this.getUnlockChestResponse(unlockedItem);

            const currentLevel = event.getOptionalData().currentLevel;
            if (!currentLevel) {
                throw new Error("Could not retrieve current level when generating unlock response.");
            }

            if (unlockedItem.getItemByItemType(GameObjectEnum.DONUT)) {
                const donutsLeftToFind = this.getDonutsLeftToFindResponse(updatedInventory, currentLevel);
                return response + donutsLeftToFind;
            }

            return response;
        }

        if (unlockedItem instanceof Door) {
            return `You use a key to unlock the door.`;
        }

        throw new Error("Was unable to generate a response for UnlockSuccessEvent.");
    }

    private getUnlockChestResponse(unlockedItem: Chest) {
        return `You use a key to unlock the ${unlockedItem.getName()}. ${unlockedItem.getContainsDescription()} 
            You add the chest contents into your inventory.`;
    }

    private getUnlockFailResponse(event: UnlockFailEvent): string {
        switch (event.getReason()) {
            case UnlockFailReasonEnum.NO_KEY:
                return "You do not have the key to open this.";
            case UnlockFailReasonEnum.NO_UNLOCKABLE_ITEM:
                return "There is no such unlockable item here.";
            case UnlockFailReasonEnum.ITEM_ALREADY_UNLOCKED:
                return "This item is already unlocked.";
            default:
                throw new Error("Was unable to generate a response for UnlockFailEvent.");
        }
    }

    private getLevelCompletedResponse(event: LevelCompletedEvent): string {
        const currentLevel = event.getCompletedLevel();
        return ResponseGenerator.generateLevelCompletedResponse(currentLevel);
    }

    private getDonutsLeftToFindResponse(inventory: Array<GameObject>, currentLevel: number): string {
        const donutService = this.getService(ServiceEnum.DonutService) as DonutService;
        const donutsLeftToFind = donutService.calculateDonutsLeftToFind(currentLevel, inventory);
        return ResponseGenerator.generateDonutsLeftToFindResponse(donutsLeftToFind);
    }
};