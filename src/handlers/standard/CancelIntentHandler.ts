import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";

export class CancelIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);
        let speechText = `See you again on Donut Dungeons!`;
        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .withShouldEndSession(true)
        .getResponse();
    }
}