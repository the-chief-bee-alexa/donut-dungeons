import { HandlerInput } from "ask-sdk-core";
import { Response, IntentRequest } from "ask-sdk-model";

import { AbstractRequestHandler } from "../../AbstractRequestHandler";
import { UserService } from "../../../services/implementations/UserService";

import { ServiceEnum } from "../../../services/ServiceEnum";
import { GameStateEnum } from "../../../objects/general_enums/GameStateEnum";

import { User } from "../../../objects/general/User";
import { GameObject } from "../../../objects/GameObject";
import { DonutService } from "../../../services/implementations/DonutService";
import { ResponseGenerator } from "../../helpers/ResponseGenerator";
import { GameObjectFactory } from "../../../objects/GameObjectFactory";
import { InitialiseLevelSuccessEvent } from "../../../objects/events/implementations/InitialiseLevelSuccessEvent";
import { InitialiseLevelFailEvent } from "../../../objects/events/implementations/InitialiseLevelFailEvent";
import { PREMIUM_LEVELS_PRODUCT_ID } from "../../in_skill_purchasing/WhatCanIBuyIntentHandler";

export enum QuestionEnum {
    BeginFirstPlay = 'BeginFirstPlay',
    StartNextLevel = 'StartNextLevel',
    BuyPremiumLevelsPackage = 'BuyPremiumLevelsPackage'
}

interface YesNoIntentSessionAttributes {
    question: string;
}

export class YesNoIntentHandler extends AbstractRequestHandler {

    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'YesNoIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        const yesOrNo = this.getYesNoSlot();
        const question = this.getQuestion();

        return this.getResponse(question, yesOrNo);
    }

    private getYesNoSlot(): string {
        const request: IntentRequest = this.getHandlerInput().requestEnvelope.request as IntentRequest;
        const intent = request.intent;
        const slots = intent.slots;

        if (!slots || !slots.YesNo.resolutions) {
            throw new Error("YesNo slot could not be found on request object!");
        }

        const resPerAuth = slots.YesNo.resolutions.resolutionsPerAuthority;
        if (!resPerAuth) {
            throw new Error("resolutionsPerAuthority could not be found on request object!");
        }

        const yesOrNo: string = resPerAuth[0].values[0].value.name;

        if (yesOrNo !== "Yes" && yesOrNo !== "No") {
            throw new Error("Slot value is not a 'Yes' or 'No'.");
        }

        return yesOrNo;
    }

    private getQuestion(): QuestionEnum {
        const sessionAttributes: YesNoIntentSessionAttributes = this.getSessionAttributes();
        const question = sessionAttributes.question;

        if (!question) {
            throw new Error("sessionAttributes.question could not be found on request object!");
        }

        return question as QuestionEnum;
    }

    private async getResponse(question: QuestionEnum, yesOrNo: string): Promise<Response> {
        let response = null;

        if (question) {
            this.removeSessionAttribute("question");
        }
       
        switch (question) {
            case QuestionEnum.BeginFirstPlay:
                response = await this.executeBeginFirstPlayQuestion(yesOrNo);
                break;
            case QuestionEnum.StartNextLevel:
                response = await this.executeStartNextLevel(yesOrNo);
                break;
            case QuestionEnum.BuyPremiumLevelsPackage:
                response = await this.executeBuyPremiumLevelsPackage(yesOrNo);
                break;
            default:
                throw new Error("sessionAttributes.question on request object does not contain a valid question.");
        }

        
        return response;
    }

    private async executeBeginFirstPlayQuestion(yesOrNo: string): Promise<Response> {
        const YES = `Alright! You've been dropped into the first dungeon. You can go left, right, climb up and down 
        ladders, look around, pick up items and open locked objects such as doors and chests. You are in the first 
        dungeon level. What will you do now?`;
        const YES_REPROMPT = "You are in the first dungeon. What will you do?";
        const NO = `OK. Don't forget to come back and try again when you're ready for the challenge! See you again on
        Donut Dungeons!`;
        
        switch (yesOrNo) {
            case 'Yes':
                const user: User | void = await this.getUser();
                if (user) {
                    throw new Error("Could not execute Yes response for question BeginFirstPlay - user already exists.");
                }

                const userId = this.getUserId();
                const userService = this.getService(ServiceEnum.UserService) as UserService;
                const newUser = await userService.createUser(userId);

                if (newUser) {
                    return this.getHandlerInput().responseBuilder
                    .speak(YES)
                    .reprompt(YES_REPROMPT)
                    .getResponse();
                }
         
                break;
            case 'No':
                return this.getHandlerInput().responseBuilder
                    .speak(NO)
                    .withShouldEndSession(true)
                    .getResponse();
            default:
                throw new Error("An invalid YesNo slot value was provided when executing the BeginFirstPlay question.");
        }

        throw new Error("Could not execute response for question 'BeginFirstPlay'.");
    }

    private async executeStartNextLevel(yesOrNo: string): Promise<Response> {
        const YES_REPROMPT = "You are now in the next dungeon. What would you like to do?";
        const NO = `OK. Don't forget to come back and try again when you're ready for the next challenge! See you again on
        Donut Dungeons!`;
        
        switch (yesOrNo) {
            case 'Yes':
                const user: User | void = await this.getUser();
                if (!user) {
                    throw new Error("Could not execute Yes response for question StartNextLevel - user does not exist.");
                }

                if (user.getGameState() === GameStateEnum.COMPLETE) {
                    const userService = this.getService(ServiceEnum.UserService) as UserService;
                    const locale = this.getHandlerInput().requestEnvelope.request.locale;
                    const serviceClientFactory = this.getHandlerInput().serviceClientFactory;

                    const initialiseLevelEvent = await userService.initialiseLevel(
                        user.getUserId(), 
                        user.getCurrentLevel() + 1,
                        serviceClientFactory,
                        locale
                    );

                    if (initialiseLevelEvent instanceof InitialiseLevelSuccessEvent) {
                        const nextLevel = user.getCurrentLevel() + 1;
                        const userAtts = await userService.getUserAttributes(user.getUserId(), ["inventory"]);
                        if (!userAtts || !userAtts.inventory) {
                            throw new Error("Could not retrieve updated inventory when executing question StartNextLevel.");
                        }

                        const inventory = this.jsonInventoryToGameObjectArray(userAtts.inventory);
                        const donutsLeftDesc = this.getDonutsLeftToFindResponse(inventory, nextLevel);

                        const YES = `Excellent! You have now been dropped into dungeon level ${nextLevel}. ${donutsLeftDesc}`;

                        return this.getHandlerInput().responseBuilder
                        .speak(YES)
                        .reprompt(YES_REPROMPT)
                        .getResponse();
                    }
                    if (initialiseLevelEvent instanceof InitialiseLevelFailEvent) {
                        const upsellMessage = `This level requires Premium Levels Package to play. Would you like to learn more?`;
                        return this.getHandlerInput().responseBuilder
                        .addDirective({
                            type: "Connections.SendRequest",
                            name: "Upsell",
                            payload: {
                                InSkillProduct: {
                                    productId: PREMIUM_LEVELS_PRODUCT_ID
                                },
                                upsellMessage: upsellMessage
                            },
                            token: "correlationToken",
                        })
                        .getResponse();
                    }
                }
                throw new Error("Could not execute Yes response for question StartNextLevel.");
            case 'No':
                return this.getHandlerInput().responseBuilder
                    .speak(NO)
                    .withShouldEndSession(true)
                    .getResponse();
            default:
                throw new Error("An invalid YesNo slot value was provided when executing the BeginFirstPlay question.");
        }
    }

    private async executeBuyPremiumLevelsPackage(yesOrNo: string): Promise<Response> {
        switch (yesOrNo) {
            case 'Yes':
                return this.getHandlerInput().responseBuilder
                .addDirective({
                    type: "Connections.SendRequest",
                    name: "Buy",
                    payload: {
                        InSkillProduct: {
                            productId: PREMIUM_LEVELS_PRODUCT_ID
                        }
                    },
                    token: "correlationToken",
                })
                .getResponse();
            case 'No':
                return this.getHandlerInput().responseBuilder
                    .speak(`Alright. What would you like to do?`)
                    .withShouldEndSession(false)
                    .getResponse();
            default:
                throw new Error("An invalid YesNo slot value was provided when executing the BeginFirstPlay question.");
        }
    }

    private jsonInventoryToGameObjectArray(jsonInventory: any): Array<GameObject> {
        const gameObjectFactory = new GameObjectFactory();
        return jsonInventory.map((itemJson: any) => {
            return gameObjectFactory.createItemFromJson(itemJson);
        });
    }

    private getDonutsLeftToFindResponse(inventory: Array<GameObject>, currentLevel: number): string {
        const donutService = this.getService(ServiceEnum.DonutService) as DonutService;
        const donutsLeftToFind = donutService.calculateDonutsLeftToFind(currentLevel, inventory);
        return ResponseGenerator.generateDonutsLeftToFindResponse(donutsLeftToFind);
    }
};