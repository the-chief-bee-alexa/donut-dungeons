import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { ServiceEnum } from "../../services/ServiceEnum";
import { UserService } from "../../services/implementations/UserService";
import { ResponseGenerator } from "../helpers/ResponseGenerator";
import { GameObject } from "../../objects/GameObject";
import { GameObjectFactory } from "../../objects/GameObjectFactory";

export class CheckInventoryIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'CheckInventoryIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        const userId = this.getUserId();

        const userService = this.getService(ServiceEnum.UserService) as UserService;
        const userAtts = await userService.getUserAttributes(userId, ["inventory"]);

        if (userAtts && userAtts.inventory) {
            const inventory = this.jsonInventoryToGameObjectArray(userAtts.inventory);
            let speechText = ResponseGenerator.generateInventoryResponse(inventory);

            return this.getHandlerInput().responseBuilder
            .speak(speechText)
            .withShouldEndSession(false)
            .getResponse(); 
        }

        throw new Error("An error occurred when calling CheckInventoryIntentHandler.");
    }

     private jsonInventoryToGameObjectArray(jsonInventory: any): Array<GameObject> {
        const gameObjectFactory = new GameObjectFactory();
        return jsonInventory.map((itemJson: any) => {
            return gameObjectFactory.createItemFromJson(itemJson);
        });
    }
}