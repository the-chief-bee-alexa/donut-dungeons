import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { questionsTable } from "./FallbackIntentHandler";

export class RepeatQuestionHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name !== 'YesNoIntent'
        && handlerInput.requestEnvelope.request.intent.name !== 'AMAZON.StopIntent'
        && handlerInput.requestEnvelope.request.intent.name !== 'AMAZON.FallbackIntent'
        && handlerInput.requestEnvelope.request.intent.name !== 'AMAZON.CancelIntent'
        && handlerInput.attributesManager.getSessionAttributes().question;
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);
        return this.getRepeatQuestion();
    }

    private getRepeatQuestion(): Response {
        const question = this.getSessionAttributes().question;
        const questionObject = questionsTable.find((questionObject: any) => {
            return questionObject.question === question;
        });

        if (!questionObject) {
            throw new Error("Could not find specified question in questionsTable.");
        }

        return this.getHandlerInput().responseBuilder
        .speak(questionObject.reprompt)
        .withShouldEndSession(false)
        .getResponse();
    }
}