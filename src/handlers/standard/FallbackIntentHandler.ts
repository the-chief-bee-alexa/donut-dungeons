import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { QuestionEnum } from "./custom/YesNoIntentHandler";

export const questionsTable = [
    {
        question: QuestionEnum.BeginFirstPlay,
        reprompt: "Would you like to begin your first play?"
    },
    {
        question: QuestionEnum.StartNextLevel,
        reprompt: "Would you like to start the next level?"
    },
    {
        question: QuestionEnum.BuyPremiumLevelsPackage,
        reprompt: "Would you be interested in buying the Premium Levels Package?"
    }
];

export class FallbackIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.FallbackIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);
        const speechText = this.getResponse();

        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .withShouldEndSession(false)
        .getResponse();
    }

    private getResponse(): string {
        let speechText = `Sorry, I didn't quite catch that. `;

        const sessionAttributes = this.getSessionAttributes();
        const question = sessionAttributes.question;
        if (question) {
            const questionReprompt = this.getQuestionReprompt(question);
            return speechText + questionReprompt;
        }

        return speechText = speechText + `Please try again.`;
    }

    private getQuestionReprompt(question: QuestionEnum): string {
        const questionObject = questionsTable.find((questionObject: any) => {
            return questionObject.question === question;
        });

        if (questionObject) {
            return questionObject.reprompt;
        }

        throw new Error("Could not retrieve question reprompt text.");
    }
}