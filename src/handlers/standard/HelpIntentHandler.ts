import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
// import { GameObject } from "../../objects/GameObject";
// import { DonutService } from "../../services/implementations/DonutService";
// import { ServiceEnum } from "../../services/ServiceEnum";
// import { ResponseGenerator } from "../helpers/ResponseGenerator";
// import { UserService } from "../../services/implementations/UserService";
// import { GameObjectFactory } from "../../objects/GameObjectFactory";

export class HelpIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        // const userId = this.getUserId();

        // const userService = this.getService(ServiceEnum.UserService) as UserService;
        // const userAtts = await userService.getUserAttributes(userId, ["currentLevel", "inventory"]);

        // if (userAtts && userAtts.currentLevel && userAtts.inventory) {
        //     const inventory = this.jsonInventoryToGameObjectArray(userAtts.inventory);
        //     let speechText = this.getResponse(inventory, userAtts.currentLevel);
        //     return this.getHandlerInput().responseBuilder
        //     .speak(speechText)
        //     .reprompt("What would you like to do now?")
        //     .getResponse();
        // }

        let speechText = `Donuts are scattered throughout the dungeons. To complete this level, 
        find all the donuts in the dungeon. Do this by moving around, either left or right, or 
        up or down ladders. Donuts may be lying around out in the open or be locked inside chests
        of various colours, you will need to find a key to open these.`;
        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .reprompt("What would you like to do now?")
        .getResponse();

        //throw new Error("An error occurred when calling HelpIntentHandler.");
    }

    // private getResponse(inventory: Array<GameObject>, currentLevel: number): string {
    //     let response = `Donuts are scattered throughout the dungeons. To complete this level, 
    //     find all the donuts in the dungeon. Do this by moving around, either left or right, or 
    //     up or down ladders. Donuts may be lying around out in the open or be locked inside chests
    //     of various colours, you will need to find a key to open these.`;

    //     return `${response} ${this.getDonutsLeftToFindResponse(inventory, currentLevel)}`;
    // }

    // // Defininitely appears in multiple places, needs refactoring...
    // private getDonutsLeftToFindResponse(inventory: Array<GameObject>, currentLevel: number): string {
    //     const donutService = this.getService(ServiceEnum.DonutService) as DonutService;
    //     const donutsLeftToFind = donutService.calculateDonutsLeftToFind(currentLevel, inventory);
    //     return ResponseGenerator.generateDonutsLeftToFindResponse(donutsLeftToFind);
    // }

    // // Needs refactoring really..
    // private jsonInventoryToGameObjectArray(jsonInventory: any): Array<GameObject> {
    //     const gameObjectFactory = new GameObjectFactory();
    //     return jsonInventory.map((itemJson: any) => {
    //         return gameObjectFactory.createItemFromJson(itemJson);
    //     });
    // }
};