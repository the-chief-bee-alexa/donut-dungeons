import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";
import { AbstractRequestHandler } from "../AbstractRequestHandler";

export class SessionEndedRequestHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;
        return request.type === "SessionEndedRequest";
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        const speechOutput = 'Goodbye.';
        return handlerInput.responseBuilder
        .speak(speechOutput)
        .getResponse();
    }
}