import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { PREMIUM_LEVELS_REF_NAME, PREMIUM_LEVELS_PRODUCT_ID } from "./WhatCanIBuyIntentHandler";

export class RefundIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'RefundIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        const locale = this.getHandlerInput().requestEnvelope.request.locale;
        const serviceClientFactory = this.getHandlerInput().serviceClientFactory;

        if (serviceClientFactory && locale) {
            const ms = serviceClientFactory.getMonetizationServiceClient();
            const productsResponse = await ms.getInSkillProducts(locale);

            const premiumLevels = productsResponse.inSkillProducts.filter(
                record => record.referenceName === PREMIUM_LEVELS_REF_NAME
            );

            if (!premiumLevels) {
                return this.getServiceUnavailableResponse();
            }

            if (premiumLevels[0].entitled !== 'ENTITLED') {
                return this.getNothingToRefundResponse();
            }

            return this.getRefundResponse();
        }

        throw new Error("Was unable to generate response for WhatCanIBuyIntent.");
    }

    private async getServiceUnavailableResponse(): Promise<Response> {
        const speechText = `This service is currently unavailable. Please try again later or contact support. What will you do next?`;

        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .withShouldEndSession(false)
        .getResponse();
    }

    private async getNothingToRefundResponse(): Promise<Response> {
        const speechText = `You do not currently own the Premium Levels Package. What will you do next?`;

        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .withShouldEndSession(false)
        .getResponse();
    }

    private async getRefundResponse(): Promise<Response> {
        return this.getHandlerInput().responseBuilder
        .addDirective({
            type: 'Connections.SendRequest',
            name: 'Cancel',
            payload: {
                InSkillProduct: {
                    productId: PREMIUM_LEVELS_PRODUCT_ID,
                }
            },
            token: "correlationToken"
        })
        .getResponse();
    }

}