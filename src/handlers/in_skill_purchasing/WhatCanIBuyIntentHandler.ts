import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";

export const PREMIUM_LEVELS_PRODUCT_ID = "amzn1.adg.product.b8aa8e44-fbb4-490d-886d-a967a4b12e9d";
export const PREMIUM_LEVELS_REF_NAME = "premium_levels";
export const PREMIUM_LEVEL_STARTER_LEVEL = 6;

export class WhatCanIBuyIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'WhatCanIBuyIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        const locale = this.getHandlerInput().requestEnvelope.request.locale;
        const serviceClientFactory = this.getHandlerInput().serviceClientFactory;

        if (serviceClientFactory && locale) {
            const ms = serviceClientFactory.getMonetizationServiceClient();
            const productsResponse = await ms.getInSkillProducts(locale);

            const premiumLevels = productsResponse.inSkillProducts.filter(
                record => record.referenceName === PREMIUM_LEVELS_REF_NAME
            );

            if (!premiumLevels) {
                return this.getServiceUnavailableResponse();
            }

            if (premiumLevels[0].entitled === 'ENTITLED') {
                return this.getAlreadyEntitledResponse();
            }

            return this.getUpsellResponse();
        }

        throw new Error("Was unable to generate response for WhatCanIBuyIntent.");
    }

    private async getServiceUnavailableResponse(): Promise<Response> {
        const speechText = `Thanks for your interest in buying the Premium Levels Package. This service is currently unavailable. 
        Please try again later or contact support. Now, what will you do next?`;

        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .withShouldEndSession(false)
        .getResponse();
    }

    private async getAlreadyEntitledResponse(): Promise<Response> {
        const speechText = `Great news, you already have access to the Premium Levels Package. You can play all free and premium dungeon levels!
        What would you like to do next?`;

        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .withShouldEndSession(false)
        .getResponse();
    }

    private async getUpsellResponse(): Promise<Response> {
        const speechText = `A Premium Levels Package is available. Would you like to learn more?`;

        return this.getHandlerInput().responseBuilder
        .addDirective({
            type: "Connections.SendRequest",
            name: "Upsell",
            payload: {
                InSkillProduct: {
                    productId: PREMIUM_LEVELS_PRODUCT_ID
                },
                upsellMessage: speechText
            },
            token: "correlationToken",
        })
        .getResponse();
    }

}