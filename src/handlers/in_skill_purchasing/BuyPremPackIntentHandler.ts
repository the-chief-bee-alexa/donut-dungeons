import { HandlerInput } from "ask-sdk-core";
import { Response } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";
import { PREMIUM_LEVELS_REF_NAME, PREMIUM_LEVELS_PRODUCT_ID } from "./WhatCanIBuyIntentHandler";

export class BuyPremPackIntentHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'BuyPremPackIntent';
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);

        const locale = this.getHandlerInput().requestEnvelope.request.locale;
        const serviceClientFactory = this.getHandlerInput().serviceClientFactory;

        if (serviceClientFactory && locale) {
            const ms = serviceClientFactory.getMonetizationServiceClient();
            const productsResponse = await ms.getInSkillProducts(locale);

            const premiumLevels = productsResponse.inSkillProducts.filter(
                record => record.referenceName === PREMIUM_LEVELS_REF_NAME
            );

            if (!premiumLevels) {
                return this.getServiceUnavailableResponse();
            }

            if (premiumLevels[0].entitled === 'ENTITLED') {
                return this.getAlreadyEntitledResponse();
            }

            return this.getBuyDirective();
        }

        throw new Error("Was unable to generate response for WhatCanIBuyIntent.");
    }

    private async getServiceUnavailableResponse(): Promise<Response> {
        const speechText = `Thanks for your interest in buying the Premium Levels Package. This service is currently unavailable. 
        Please try again later or contact support. Now, what will you do next?`;

        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .withShouldEndSession(false)
        .getResponse();
    }

    private async getAlreadyEntitledResponse(): Promise<Response> {
        const speechText = `Great news, you have already bought the Premium Levels Package. You can play all free and premium dungeon levels!
        What would you like to do next?`;

        return this.getHandlerInput().responseBuilder
        .speak(speechText)
        .withShouldEndSession(false)
        .getResponse();
    }

    private async getBuyDirective(): Promise<Response> {
        return this.getHandlerInput().responseBuilder
        .addDirective({
            type: "Connections.SendRequest",
            name: "Buy",
            payload: {
                InSkillProduct: {
                    productId: PREMIUM_LEVELS_PRODUCT_ID
                }
            },
            token: "correlationToken",
        })
        .getResponse();
    }

}