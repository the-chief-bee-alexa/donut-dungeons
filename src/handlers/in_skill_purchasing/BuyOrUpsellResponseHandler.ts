import { HandlerInput } from "ask-sdk-core";
import { Response, interfaces } from "ask-sdk-model";

import { AbstractRequestHandler } from "../AbstractRequestHandler";

export class BuyOrUpsellResponseHandler extends AbstractRequestHandler {
    canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'Connections.Response' && (request.name === 'Buy' || request.name === 'Upsell');
    }

    async handle(handlerInput: HandlerInput): Promise<Response> {
        this.setHandlerInput(handlerInput);
        this.addSessionAttribute("SKILL_STARTED", "SKILL_STARTED");
        return this.getResponse();
    }

    private getResponse(): Response {
        const conResp = this.getConnectionsResponse();
        if (conResp.status && conResp.status.code && conResp.payload) {
            if (conResp.status.code === '200') {
                let speakOutput;
                const repromptOutput = 'What would you like to do next?';
                const errorOutput = "";
                const purchaseResult = conResp.payload.purchaseResult;

                switch (purchaseResult) {
                    case 'ACCEPTED':
                        // TODO: add buy record in database
                        speakOutput = 'You now have access to all premium levels. What would you like to do next?';
                        break;
                    case 'DECLINED':
                        speakOutput = 'What would you like to do next?';
                        break;
                    case 'ALREADY_PURCHASED':
                        speakOutput = `Thanks for supporting us. Be sure to look out for new levels added in the future. What would you like to do next?`;
                        break;
                    case 'ERROR':
                        speakOutput = `This service is temporarily unavailable. What would you like to do next?`;
                        break;
                    default:
                        console.log(`unhandled purchaseResult: ${conResp.payload.purchaseResult}`);
                        speakOutput = errorOutput;
                        break;
                }

                return this.getHandlerInput().responseBuilder
                .speak(speakOutput)
                .reprompt(repromptOutput)
                .withShouldEndSession(false)
                .getResponse();
            }
        }

        const errorOutput = `Something unexpected happened while making your purchase. Thank you for your interest in buying the Premium Levels Pacakge, 
        please try again later. What would you like to do next?`;
        return this.getHandlerInput().responseBuilder
        .speak(errorOutput)
        .withShouldEndSession(false)
        .getResponse();
    }

    private getConnectionsResponse(): interfaces.connections.ConnectionsResponse {
        return this.getHandlerInput().requestEnvelope.request as interfaces.connections.ConnectionsResponse;
    }
}