import levelOne from "./1.json";
import levelTwo from "./2.json";
import levelThree from "./3.json";
import levelFour from "./4.json";
import levelFive from "./5.json";

// Premium Levels
import levelSix from "./6.json";
import levelSeven from "./7.json";
import levelEight from "./8.json";
import levelNine from "./9.json";
import levelTen from "./10.json";

type LevelData = [any, string];
export const levelSelector = (level: number): LevelData => {
    switch (level) {
        case 1:
            return [levelOne, "1,1"];
        case 2:
            return [levelTwo, "1,1"];
        case 3:
            return [levelThree, "1,1"];
        case 4:
            return [levelFour, "1,1"];
        case 5:
            return [levelFive, "1,1"];
        case 6:
            return [levelSix, "1,2"];
        case 7:
            return [levelSeven, "2,1"];
        case 8:
            return [levelEight, "1,1"];
        case 9:
            return [levelNine, "2,2"];
        case 10:
            return [levelTen, "4,3"];
        default:
            throw new Error("Level selector could not retrieve specified level.");
    }
}