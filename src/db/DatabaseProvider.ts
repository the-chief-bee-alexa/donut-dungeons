import { DynamoDB } from "aws-sdk";
import { AbstractAccessor } from "./AbstractAccessor";
import { UserAccessor } from "./accessors/UserAccessor";
import { AccessorEnum } from "./AccessorEnum";
import { CellAccessor } from "./accessors/CellAccessor";
import { GameObjectFactory } from "../objects/GameObjectFactory";

export class DatabaseProvider {

    private dbClient:  DynamoDB.DocumentClient;
    private tableName: string;
    private accessors: Array<AbstractAccessor> = [];

    constructor(dbClient: DynamoDB.DocumentClient, tableName: string) {
        this.dbClient = dbClient;
        this.tableName = tableName;
     
        this.addAccessors();
    }

    private addAccessors(): void {
        this.accessors.push(new UserAccessor(this.dbClient, this.tableName));
        this.accessors.push(new CellAccessor(this.dbClient, this.tableName, new GameObjectFactory()));
    }

    public getAccessor(accessorName: AccessorEnum): AbstractAccessor {
        const accessor = this.accessors.find((element) => {    
            return element.getName() === accessorName;
        });

        if (accessor) {
            return accessor;
        }

        throw new Error("Database accessor not recognised.");
    }
}