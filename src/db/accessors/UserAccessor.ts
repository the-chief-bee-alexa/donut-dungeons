import { DynamoDB } from "aws-sdk";
import { AbstractAccessor } from "../AbstractAccessor";
import { User } from "../../objects/general/User";
import { UserAttributes } from "../../objects/general/UserAttributes";

import { GameStateEnum } from "../../objects/general_enums/GameStateEnum";
import { AccessorEnum } from "../AccessorEnum";
import { levelSelector } from "../../levels/levelSelector";

export class UserAccessor extends AbstractAccessor {

    constructor(dbClient: DynamoDB.DocumentClient, tableName: string) {
        super(dbClient, tableName);
    }

    public getName(): AccessorEnum {
        return AccessorEnum.UserAccessor;
    }

    public async addUser(user: User): Promise<any> {
        const params = {
            TableName: this.tableName,
            Item: {
                'userId': user.getUserId(),
                'username': user.getUsername(),
                'currentLevel': user.getCurrentLevel(),
                'currentLocation': user.getCurrentLocation().toString(),
                'worldState': user.getWorldState(),
                'inventory': user.getInventory(),
                'joinDate': user.getJoinDate(),
                'lastLoginDate': user.getLastLoginDate(),
                'loginCount': user.getLoginCount(),
                'gameState': user.getGameState(),
                'completedLevels': user.getCompletedLevels()
            }
        };

        return this.dbClient.put(params).promise();
    }

    public async getUserById(userId: string): Promise<User | void> {
        const params = {
            TableName: this.tableName,
            Key: {
              'userId': userId
            }
        };

        const response = await this.dbClient.get(params).promise();
        const userJsonObject = response.Item;

        if (userJsonObject) {
            return User.createUserFromJson(userJsonObject);
        }
    }

    public async recordLogin(userId: string, lastLoginDate: number): Promise<boolean> {
        const params = {
            TableName: this.tableName,
            Key: {
                "userId": userId
            },
            UpdateExpression: "set lastLoginDate = :lastLoginDate, loginCount = loginCount + :incrementAmount",
            ExpressionAttributeValues: {
                ":lastLoginDate": lastLoginDate,
                ":incrementAmount": 1
            },
            ReturnValues: "UPDATED_NEW"
        };

        const response = await this.dbClient.update(params).promise();
        const userJsonObject = response.Attributes;

        return userJsonObject && userJsonObject.lastLoginDate && userJsonObject.loginCount;
    }

    public async setUserLocation(userId: string, newLocation: string): Promise<string | void> {
        const params = {
            TableName: this.tableName,
            Key: {
                "userId": userId
            },
            UpdateExpression: "set currentLocation = :currentLocation",
            ExpressionAttributeValues: {
                ":currentLocation": newLocation.toString()
            },
            ReturnValues: "UPDATED_NEW"
        };

        const response = await this.dbClient.update(params).promise();
        const userLocJsonObject = response.Attributes;

        if (userLocJsonObject && userLocJsonObject.currentLocation) {
            return userLocJsonObject.currentLocation;
        }
    }

    public async getUserAttributes(userId: string, attributes: Array<string>): Promise<UserAttributes | void> {
        if (attributes.length < 1) {
            throw new Error("You must provide at least one attribute to call getUserAttributes.");
        }

        const atts = attributes.join(",");
        const params = {
            TableName: this.tableName,
            Key: {
              'userId': userId
            },
            ProjectionExpression: atts
        };

        const response = await this.dbClient.get(params).promise();
        const userAttributeJsonObject = response.Item;

        if (userAttributeJsonObject) {
            return userAttributeJsonObject;
        }
    }

    public async completeLevel(userId: string, completedLevel: number): Promise<boolean> {
        const params = {
            TableName: this.tableName,
            Key: {
                "userId": userId
            },
            UpdateExpression: "set gameState = :gameState, completedLevels = list_append(completedLevels,:completedLevel)",
            ExpressionAttributeValues: {
                ":gameState": GameStateEnum.COMPLETE,
                ":completedLevel": [completedLevel]
            },
            ReturnValues: "UPDATED_NEW"
        };

        const response = await this.dbClient.update(params).promise();
        const userJsonObject = response.Attributes;

        return userJsonObject && userJsonObject.gameState && userJsonObject.completedLevels;
    }

    public async initialiseLevel(userId: string, level: number): Promise<boolean> {
        const updateCurrentLevel = 'currentLevel = :currentLevel';
        const updateCurrentLocation = 'currentLocation = :currentLocation';
        const updateInventory = 'inventory = :inventory';
        const updateGameState = 'gameState = :gameState';
        const updateWorldState = 'worldState = :worldState';

        const levelData = levelSelector(level);
        const file = levelData["0"];
        const levelStartLocation = levelData["1"];

        if (!file || !levelStartLocation) {
            return false;
        }

        const params = {
            TableName: this.tableName,
            Key: {
                "userId": userId
            },
            UpdateExpression: `set ${updateCurrentLevel}, ${updateCurrentLocation}, ${updateInventory}, ${updateGameState}, ${updateWorldState}`,
            ExpressionAttributeValues: {
                ":currentLevel": level,
                ":currentLocation": levelStartLocation,
                ":inventory": [],
                ":gameState": GameStateEnum.IN_PROGRESS,
                ":worldState": file
            },
            ReturnValues: "UPDATED_NEW"
        };

        const response = await this.dbClient.update(params).promise();
        const userJsonObject = response.Attributes;

        return userJsonObject && userJsonObject.currentLevel && userJsonObject.currentLocation && userJsonObject.inventory 
            && userJsonObject.gameState && userJsonObject.worldState;
    }
}