import { DynamoDB } from "aws-sdk";

import { AccessorEnum } from "../AccessorEnum";
import { DirectionEnum } from "../../objects/general_enums/DirectionEnum";

import { AbstractAccessor } from "../AbstractAccessor";
import { GameObjectFactory } from "../../objects/GameObjectFactory";
import { GameObject } from "../../objects/GameObject";
import { Cell } from "../../objects/structures/Cell";

export class CellAccessor extends AbstractAccessor {

    private gameObjectFactory: GameObjectFactory;

    constructor(dbClient: DynamoDB.DocumentClient, tableName: string, gameObjectFactory: GameObjectFactory) {
        super(dbClient, tableName);
        this.gameObjectFactory = gameObjectFactory;
    }

    public getName(): AccessorEnum {
        return AccessorEnum.CellAccessor;
    }

    public async getCell(userId: string, cellCoords: string): Promise<Cell | void> {
        const xy: [number, number] = this.stringToNumberNumberIndexTuple(cellCoords);
        const params = {
            TableName: this.tableName,
            Key: {
              'userId': userId
            },
            ProjectionExpression: 'worldState['+ xy["1"] +']['+ xy["0"] +']'
        };
     
        const response = await this.dbClient.get(params).promise();
        const item = response.Item;

        if (item && item.worldState[0][0]) {
            return this.gameObjectFactory.createStructureFromJson(item.worldState[0][0]) as Cell;
        }
    }

    public async pickUpItem(
        userId: string,
        cellCoords: string,
        updatedCellItems: Array<GameObject>,
        updatedInventoryItems: Array<GameObject>): Promise<Array<GameObject> | void> {
            const xy: [number, number] = this.stringToNumberNumberIndexTuple(cellCoords);

            const updateWorldStateCellItems = 'worldState[' + xy["1"] +']['+ xy["0"] +'].#items = :cellItems';
            const updateInventory = 'inventory = :inventoryItems';
            const updateExpression = `set ${updateWorldStateCellItems}, ${updateInventory}`;

            const params = {
                TableName: this.tableName,
                Key: {
                    'userId': userId
                },
                UpdateExpression: updateExpression,
                ExpressionAttributeNames: {
                    '#items': 'items',
                },
                ExpressionAttributeValues: {
                    ':cellItems': updatedCellItems,
                    ':inventoryItems': updatedInventoryItems
                },
                ReturnValues: 'UPDATED_NEW'
            };

            const response = await this.dbClient.update(params).promise();
            const itemsJsonObject = response.Attributes;

            if (itemsJsonObject && itemsJsonObject.inventory && itemsJsonObject.worldState) {
                return itemsJsonObject.inventory.map((item: any) => {
                    return this.gameObjectFactory.createItemFromJson(item);
                });
            }
    }

    public async unlockChest(
        userId: string,
        cellCoords: string,
        chestItemIndex: number,
        updatedContains: Array<GameObject>,
        updatedInventory: Array<GameObject>): Promise<boolean> {
            const xy: [number, number] = this.stringToNumberNumberIndexTuple(cellCoords);

            const item = `worldState[${xy["1"]}][${xy["0"]}].#items[${chestItemIndex}]`;

            const updateChestContains = `${item}.contains = :contains`;
            const updateChestIsLocked = `${item}.isLocked = :isLocked`;
            const updateInventory = 'inventory = :inventory';
            const updateExpression = `set ${updateChestContains}, ${updateChestIsLocked}, ${updateInventory}`;

            const params = {
                TableName: this.tableName,
                Key: {
                    'userId': userId
                },
                UpdateExpression: updateExpression,
                ExpressionAttributeNames: {
                    '#items': 'items',
                },
                ExpressionAttributeValues: {
                    ':contains': updatedContains,
                    ':isLocked': false,
                    ':inventory': updatedInventory
                },
                ReturnValues: 'UPDATED_NEW'
            };
    
            const response = await this.dbClient.update(params).promise();
            const itemsJsonObject = response.Attributes;

            return itemsJsonObject && itemsJsonObject.inventory && itemsJsonObject.worldState;
    }

    public async unlockDoor(
        userId: string,
        cellCoords: string,
        doorDirection: DirectionEnum,
        updatedInventory: Array<GameObject>): Promise<boolean> {
            const xy: [number, number] = this.stringToNumberNumberIndexTuple(cellCoords);
            const x = xy["0"];
            const y = xy["1"];

            const item = `worldState[${y}][${x}].#itemDirection`;

            const adjacentX = doorDirection === DirectionEnum.LEFT ? x - 1 : x + 1;
            const adjacentDirection = doorDirection === DirectionEnum.LEFT ? DirectionEnum.RIGHT : DirectionEnum.LEFT;
            const adjacentCellItem = `worldState[${y}][${adjacentX}].#adjacentItemDirection`;

            const updateDoorIsLocked = `${item}.isLocked = :isLocked`;
            const updateDoorIsLockedAdjacent = `${adjacentCellItem}.isLocked = :isLocked`;
            const updateInventory = 'inventory = :inventory';
            const updateExpression = `set ${updateDoorIsLocked}, ${updateDoorIsLockedAdjacent}, ${updateInventory}`;

            const params = {
                TableName: this.tableName,
                Key: {
                    'userId': userId
                },
                UpdateExpression: updateExpression,
                ExpressionAttributeNames: {
                    '#itemDirection': doorDirection,
                    '#adjacentItemDirection': adjacentDirection
                },
                ExpressionAttributeValues: {
                    ':isLocked': false,
                    ':inventory': updatedInventory
                },
                ReturnValues: 'UPDATED_NEW'
            };
    
            const response = await this.dbClient.update(params).promise();
            const itemsJsonObject = response.Attributes;

            return itemsJsonObject && itemsJsonObject.inventory && itemsJsonObject.worldState;
    }

    private stringToNumberNumberIndexTuple(cellCoords: string): [number, number] {
        const coords = cellCoords.split(",");
        if (!coords[0] || !coords[1]) {
            throw new Error("Not a valid pair of Cell coordinates.");
        }
      
        const x = Number(coords[0]) - 1;
        const y = Number(coords[1]) - 1;
        return [x, y];
    }

}
