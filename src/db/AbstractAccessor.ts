import { DynamoDB } from "aws-sdk";
import { AccessorEnum } from "./AccessorEnum";

export abstract class AbstractAccessor {
    protected tableName: string;
    protected dbClient: DynamoDB.DocumentClient;

    constructor(dbClient: DynamoDB.DocumentClient, tableName: string) {
        this.tableName = tableName;
        this.dbClient = dbClient;
    }

    abstract getName(): AccessorEnum;
}