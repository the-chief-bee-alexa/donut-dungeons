import { GameStateEnum } from "../general_enums/GameStateEnum";

export class User {

    private userId: string;
    private username: string | null;
    private currentLevel: number;
    private currentLocation: string;
    private worldState: any;
    private inventory: any;
    private joinDate: number;
    private lastLoginDate: number;
    private loginCount: number;
    private gameState: GameStateEnum;
    private completedLevels: Array<number>;

    constructor(
        userId: string,
        currentLevel: number,
        currentLocation: string,
        worldState: any,
        inventory: any,
        joinDate: number,
        lastLoginDate: number,
        loginCount: number,
        gameState: GameStateEnum,
        completedLevels: Array<number>,
        username?: string,
    ) {
        this.userId = userId;
        this.currentLevel = currentLevel;
        this.currentLocation = currentLocation;
        this.worldState = worldState;
        this.inventory = inventory;
        this.joinDate = joinDate;
        this.lastLoginDate = lastLoginDate;
        this.loginCount = loginCount;
        this.gameState = gameState;
        this.completedLevels = completedLevels;
        this.username = username ? username : null;
    }

    public setUserId(userId: string): void {
        this.userId = userId;
    }

    public getUserId(): string {
        return this.userId;
    }

    public setUsername(username: string): void {
        this.username = username;
    }

    public getUsername(): string | null {
        return this.username;
    }

    public setCurrentLevel(currentLevel: number): void {
        this.currentLevel = currentLevel;
    }

    public getCurrentLevel(): number {
        return this.currentLevel;
    }

    public setCurrentLocation(currentLocation: string): void {
        this.currentLocation = currentLocation;
    }

    public getCurrentLocation(): string {
        return this.currentLocation;
    }

    public setWorldState(worldState: any): void {
        this.worldState = worldState;
    }

    public getWorldState(): any {
        return this.worldState;
    }

    public setInventory(inventory: any): void {
        this.inventory = inventory;
    }

    public getInventory(): any {
        return this.inventory;
    }

    public setJoinDate(joinDate: number): void {
        this.joinDate = joinDate;
    }

    public getJoinDate(): number {
        return this.joinDate;
    }

    public setLastLoginDate(lastLoginDate: number): void {
        this.lastLoginDate = lastLoginDate;
    }

    public getLastLoginDate(): number {
        return this.lastLoginDate;
    }

    public setLoginCount(loginCount: number): void {
        this.loginCount = loginCount;
    }

    public getLoginCount(): number {
        return this.loginCount;
    }

    public setGameState(gameState: GameStateEnum): void {
        this.gameState = gameState;
    }

    public getGameState(): GameStateEnum {
        return this.gameState;
    }

    public setCompletedLevels(completedLevels: Array<number>): void {
        this.completedLevels = completedLevels;
    }

    public getCompletedLevels(): Array<number> {
        return this.completedLevels;
    }

    public static createUserFromJson(userJsonObject: any) {
        return new User(
            userJsonObject.userId,
            userJsonObject.currentLevel,
            userJsonObject.currentLocation,
            userJsonObject.worldState,
            userJsonObject.inventory,
            userJsonObject.joinDate,
            userJsonObject.lastLoginDate,
            userJsonObject.loginCount,
            userJsonObject.gameState,
            userJsonObject.completedLevels,
            userJsonObject.username
        );
    }
}