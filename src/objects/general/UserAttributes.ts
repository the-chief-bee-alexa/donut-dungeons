import { GameObject } from "../GameObject";

export interface UserAttributes {
     userId?: string;
     username?: string | null;
     currentLevel?: number;
     currentLocation?: string;
     worldState?: Array<GameObject>;
     inventory?: Array<GameObject>;
     joinDate?: number;
     lastLoginDate?: number;
     loginCount?: number;
     gameState?: string;
     completedLevels?: Array<number>;
}