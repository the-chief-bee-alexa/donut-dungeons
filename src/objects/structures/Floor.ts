import { GameObject } from "../GameObject";
import { GameObjectEnum } from "../GameObjectEnum";

export class Floor extends GameObject {
    constructor() {
        super(GameObjectEnum.FLOOR, "Floor");
    }
}