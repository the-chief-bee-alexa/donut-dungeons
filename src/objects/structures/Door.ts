import { GameObject } from "../GameObject";
import { GameObjectEnum } from "../GameObjectEnum";

export class Door extends GameObject {

    private leadsTo: string;
    private isLocked: boolean;
    private openedWithKeyId: number;

    constructor(
        name: string,
        leadsTo: string,
        isLocked: boolean,
        openedWithKeyId: number
    ) {
        super(GameObjectEnum.DOOR, name);
        this.leadsTo = leadsTo;
        this.isLocked = isLocked;
        this.openedWithKeyId = openedWithKeyId;
    }

    public setLeadsTo(leadsTo: string): void {
        this.leadsTo = leadsTo;
    }

    public getLeadsTo(): string {
        return this.leadsTo;
    }

    public setIsLocked(locked: boolean) {
        this.isLocked = locked;
    }

    public getIsLocked(): boolean {
        return this.isLocked;
    }

    public setOpenedWithKeyId(openedWithKeyId: number) {
        this.openedWithKeyId = openedWithKeyId;
    }

    public getOpenedWithKeyId(): number {
        return this.openedWithKeyId;
    }
}