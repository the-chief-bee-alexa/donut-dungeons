import { GameObject } from "../GameObject";
import { GameObjectEnum } from "../GameObjectEnum";

export class Wall extends GameObject {
    constructor() {
        super(GameObjectEnum.WALL, "Wall");
    }
}