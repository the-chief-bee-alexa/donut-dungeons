import { GameObject } from "../GameObject";
import { GameObjectEnum } from "../GameObjectEnum";

export class Corridor extends GameObject {

    private leadsTo: string;

    constructor(leadsTo: string) {
        super(GameObjectEnum.CORRIDOR, "corridor");
        this.leadsTo = leadsTo;
    }

    public setLeadsTo(leadsTo: string): void {
        this.leadsTo = leadsTo;
    }

    public getLeadsTo(): string {
        return this.leadsTo;
    }
}