import { DirectionEnum } from "../general_enums/DirectionEnum";
import { GameObjectEnum } from "../GameObjectEnum";

import { GameObject } from "../GameObject";
import { Door } from "./Door";
import { Chest } from "../items/Chest";

export class Cell extends GameObject {

    private coords: string;
    private top: GameObject;
    private right: GameObject;
    private bottom: GameObject;
    private left: GameObject;
    private items: Array<GameObject>;

    constructor(
        coords: string,
        top: GameObject,
        right: GameObject,
        bottom: GameObject,
        left: GameObject,
        items: Array<GameObject>
    ) {
        super(GameObjectEnum.CELL, "cell");
        this.coords = coords;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
        this.left = left;
        this.items = items;
    }

    public setCoords(coords: string): void {
        this.coords = coords;
    }

    public getCoords(): string {
        return this.coords;
    }

    public setTop(top: GameObject): void {
        this.top = top;
    }

    public getTop(): GameObject {
        return this.top;
    }

    public setRight(right: GameObject): void {
        this.right = right;
    }

    public getRight(): GameObject {
        return this.right;
    }

    public setBottom(bottom: GameObject): void {
        this.bottom = bottom;
    }

    public getBottom(): GameObject {
        return this.bottom;
    }

    public setLeft(left: GameObject): void {
        this.left = left;
    }

    public getLeft(): GameObject {
        return this.left;
    }

    public setItems(items: Array<GameObject>): void {
        this.items = items;
    }

    public getItems(): Array<GameObject> {
        return this.items;
    }

    public isValidMove(direction: DirectionEnum): boolean {
        const gameObject = this.getGameObjectInDirection(direction);
        return this.allowsMovement(gameObject);
    }

    public getGameObjectInDirection(direction: DirectionEnum): GameObject {
        switch (direction) {
            case DirectionEnum.UP:
                return this.top;
            case DirectionEnum.RIGHT:
                return this.right;
            case DirectionEnum.DOWN:
                return this.bottom;
            case DirectionEnum.LEFT:
                return this.left;
        }

        throw new Error("Given Direction to move not recognised.");
    }

    public getDescription(): string {
        const structuresDescription = this.getStructuresDescription();
        const itemsDescription = this.getItemsDescription();

        if (structuresDescription !== "" || itemsDescription !== "") {
            return `${structuresDescription} ${itemsDescription}`;
        }

        return "";
    }

    public findItem(itemType: GameObjectEnum): [GameObject, number] | void {
        let res: [GameObject, number] | null = null;
        this.items.find((item: GameObject, index: number) => {
            if (item.getType() === itemType) {
                res = [item, index];
                return true;
            }
            return false;
        });

        if (res) return res;
    }

    public findStructure(itemType: GameObjectEnum): [GameObject, DirectionEnum] | void {
        let res: [GameObject, DirectionEnum] | null = null;
        const surroundingStructures = [
            this.top,
            this.right,
            this.bottom,
            this.left
        ];

        surroundingStructures.find((structure: GameObject, index: number) => {
            if (structure.getType() === itemType) {
                const direction = this.getDirectionByIndexValue(index);
                res = [structure, direction];
                return true;
            }
            return false;
        });

        if (res) return res;
    }

    private getDirectionByIndexValue(index: number): DirectionEnum {
        switch (index) {
            case 0:
                return DirectionEnum.UP;
            case 1:
                return DirectionEnum.RIGHT;
            case 2:
                return DirectionEnum.DOWN;
            case 3:
                return DirectionEnum.LEFT;
            default:
                throw new Error("An index value of 0-3 must be provided to get direction by index value.");
        }
    }

    private getStructuresDescription() : string {
        let desc = "";
        const surroundingStructures = [
            this.top,
            this.right,
            this.bottom,
            this.left
        ];

        surroundingStructures.forEach((structure: GameObject, index: number) => {
            const structureType = structure.getType();
            const structureName = structure.getName();

            switch (structureType) {
                case GameObjectEnum.LADDER:
                    const upOrDownwards = index === 2 ? "downwards" : "upwards";
                    desc = desc + `There is a ${structureName} going ${upOrDownwards}. `;

                    break;
                case GameObjectEnum.DOOR:
                    const direction = index === 1 ? "right" : "left";
                    desc = desc + `You notice a ${structureName} to the ${direction}. `;

                    break;
                default:
                    break;
            }
        });

        return desc;
    }

    private getItemsDescription(): string {
        let desc = "";
        const items = this.getItems();
        items.forEach((item: GameObject) => {
            switch (item.getType()) {
                case GameObjectEnum.CHEST:
                    const chest = item as Chest;
                    const chestDescription = chest.getDescription();
                    desc = desc + `${chestDescription} `;
                    break;
                case GameObjectEnum.KEY:
                    desc = desc + `You see a ${item.getName()} on the ground. `;
                    break;
                case GameObjectEnum.DONUT:
                    desc = desc + `There is a delicious looking ${item.getName()}. `;
                    break;
                default:
                    break;
            }
        });

        return desc;
    }

    private allowsMovement(gameObject: GameObject): boolean {
        const objectType = gameObject.getType();
        if (objectType === GameObjectEnum.CORRIDOR || 
            objectType === GameObjectEnum.LADDER) {
            return true;
        }

        if (objectType === GameObjectEnum.FLOOR || 
            objectType === GameObjectEnum.WALL || 
            objectType === GameObjectEnum.CEILING) {
            return false;
        }

        if (objectType === GameObjectEnum.DOOR) {
            const door = gameObject as Door;
            const isLocked = door.getIsLocked();
            return !isLocked;
        }

        throw new Error("Could not recognise type of given GameObject when moving.");
    }
}