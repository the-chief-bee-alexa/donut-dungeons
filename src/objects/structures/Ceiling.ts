import { GameObject } from "../GameObject";
import { GameObjectEnum } from "../GameObjectEnum";

export class Ceiling extends GameObject {
    constructor() {
        super(GameObjectEnum.CEILING, "Ceiling");
    }
}