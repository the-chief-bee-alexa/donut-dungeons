export enum GameObjectEnum {
    CELL = 'CELL',
    CORRIDOR = 'CORRIDOR',
    LADDER = 'LADDER',
    DOOR = 'DOOR',
    FLOOR = 'FLOOR',
    WALL = 'WALL',
    CEILING = 'CEILING',
    KEY = 'KEY',
    CHEST = 'CHEST',
    DONUT = 'DONUT'
}