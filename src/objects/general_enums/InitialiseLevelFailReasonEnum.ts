export enum InitialiseLevelFailReasonEnum {
    LEVEL_NOT_REACHED = 'LEVEL_NOT_REACHED',
    USER_NOT_ENTITLED = 'USER_NOT_ENTITLED'
}