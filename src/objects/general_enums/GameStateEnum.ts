export enum GameStateEnum {
    IN_PROGRESS = 'IN_PROGRESS',
    COMPLETE = 'COMPLETE'
}