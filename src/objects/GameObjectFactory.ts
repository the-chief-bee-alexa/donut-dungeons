import { GameObjectEnum } from "./GameObjectEnum";
import { GameObject } from "./GameObject";

import { Ladder } from "./structures/Ladder";
import { Door } from "./structures/Door";
import { Cell } from "./structures/Cell";
import { Chest } from "./items/Chest";
import { Key } from "./items/Key";
import { Donut } from "./items/Donut";
import { Corridor } from "./structures/Corridor";
import { Ceiling } from "./structures/Ceiling";
import { Wall } from "./structures/Wall";
import { Floor } from "./structures/Floor";

export class GameObjectFactory {

    public createStructureFromJson(gameObjectJson: any): GameObject {
        switch (gameObjectJson.type) {
            case GameObjectEnum.CEILING:
                return this.createCeiling();
            case GameObjectEnum.WALL:
                return this.createWall();
            case GameObjectEnum.FLOOR:
                return this.createFloor();
            case GameObjectEnum.CORRIDOR:
                return this.createCorridor(gameObjectJson);
            case GameObjectEnum.LADDER:
                return this.createLadder(gameObjectJson);
            case GameObjectEnum.DOOR:
                return this.createDoor(gameObjectJson);
            case GameObjectEnum.CELL:
                return this.createCell(gameObjectJson);
        }
        
        throw new Error("Could not create structure GameObject from given JSON.");
    }

    public createItemFromJson(gameObjectJson: any): GameObject {
        switch (gameObjectJson.type) {
            case GameObjectEnum.CHEST:
                return this.createChest(gameObjectJson);
            case GameObjectEnum.KEY:
                return this.createKey(gameObjectJson);
            case GameObjectEnum.DONUT:
                return this.createDonut(gameObjectJson);
        }

        throw new Error("Could not create item GameObject from given JSON.");
    }

    private createCeiling(): GameObject {
            return new Ceiling();
    }

    private createWall(): GameObject {
        return new Wall();
    }

    private createFloor(): GameObject {
        return new Floor();
    }

    private createCorridor(gameObjectJson: any): GameObject {
        return new Corridor(gameObjectJson.leadsTo);
    }

    private createLadder(gameObjectJson: any): GameObject {
        return new Ladder(gameObjectJson.leadsTo);
    }

    private createDoor(gameObjectJson: any): GameObject {
        return new Door(
            gameObjectJson.name, 
            gameObjectJson.leadsTo,
            gameObjectJson.isLocked,
            gameObjectJson.openedWithKeyId
        );
    }

    private createChest(gameObjectJson: any): GameObject {
        const contains = this.createItems(gameObjectJson.contains);

        return new Chest(
            gameObjectJson.name, 
            gameObjectJson.isLocked, 
            contains,
            gameObjectJson.openedWithKeyId
        );
    }

    private createKey(gameObjectJson: any): GameObject {
        return new Key(
            gameObjectJson.keyId,
            gameObjectJson.name
        );
    }

    private createDonut(gameObjectJson: any): GameObject {
        return new Donut(gameObjectJson.name, gameObjectJson.quantity);
    }

    private createCell(gameObjectJson: any): GameObject {
        if (this.containsCell(gameObjectJson)) {
            throw new Error("A Cell cannot contain another Cell.");
        }

        const items = this.createItems(gameObjectJson.items);

        return new Cell(
            gameObjectJson.coords,
            this.createStructureFromJson(gameObjectJson.top),
            this.createStructureFromJson(gameObjectJson.right),
            this.createStructureFromJson(gameObjectJson.bottom),
            this.createStructureFromJson(gameObjectJson.left),
            items
        );
    }

    private containsCell(gameObjectJson: any): boolean {
        const cell = GameObjectEnum.CELL;
        return gameObjectJson.top === cell || gameObjectJson.right === cell || 
            gameObjectJson.bottom === cell || gameObjectJson.left === cell;
    }

    private createItems(gameObjectJson: any): Array<GameObject> {
        let items = gameObjectJson.map((item: any) => {
            return this.createItemFromJson(item);
        });

        if (!items) {
            items = [];
        }

        return items;
    }
}