import { GameObjectEnum } from "./GameObjectEnum";

export abstract class GameObject {

    private type: GameObjectEnum;
    private name: string;

    constructor(type: GameObjectEnum, name: string) {
        this.type = type;
        this.name = name;
    }

    public setType(type: GameObjectEnum) {
        this.type = type;
    }

    public getType(): GameObjectEnum {
        return this.type;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getName(): string {
        return this.name;
    }
}