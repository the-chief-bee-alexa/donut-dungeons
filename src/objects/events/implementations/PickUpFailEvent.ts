import { EventEnum } from "../EventEnum";
import { PickUpFailReasonEnum } from "../../general_enums/PickUpFailReasonEnum";

export interface PickUpFailEventPayloadInterface {
    reason: PickUpFailReasonEnum;
}

export class PickUpFailEvent {
    private payload: PickUpFailEventPayloadInterface;

    constructor(payload: PickUpFailEventPayloadInterface) {
        this.payload = payload;
    }

    public getName(): string {
        return EventEnum.PICK_UP_FAIL;
    }

    public getReason(): PickUpFailReasonEnum {
        return this.payload.reason;
    }
}