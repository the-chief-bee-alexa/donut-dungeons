import { EventEnum } from "../EventEnum";
import { GameObject } from "../../GameObject";

export interface UnlockSuccessOptionalData {
    currentLevel?: number;
}

export interface UnlockSuccessEventPayloadInterface {
    unlockedItem: GameObject;
    updatedInventory: Array<GameObject>;
    optionalData: UnlockSuccessOptionalData;
}

export class UnlockSuccessEvent {
    private payload: UnlockSuccessEventPayloadInterface;

    constructor(payload: UnlockSuccessEventPayloadInterface) {
        this.payload = payload;
    }

    public getName(): string {
        return EventEnum.UNLOCK_SUCCESS;
    }

    public getUnlockedItem(): GameObject {
        return this.payload.unlockedItem;
    }

    public getUpdatedInventory(): Array<GameObject> {
        return this.payload.updatedInventory;
    }

    public setOptionalData(optionalData: UnlockSuccessOptionalData) {
        this.payload.optionalData = optionalData;
    }

    public getOptionalData(): UnlockSuccessOptionalData {
        return this.payload.optionalData;
    }
}