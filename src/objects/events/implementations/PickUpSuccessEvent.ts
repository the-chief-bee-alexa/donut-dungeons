import { GameObject } from "../../GameObject";
import { EventEnum } from "../EventEnum";

export interface PickUpSuccessEventPayloadInterface {
    pickedUpItem: GameObject;
    updatedInventory: Array<GameObject>;
    currentLevel: number;
}

export class PickUpSuccessEvent {
    
    private payload: PickUpSuccessEventPayloadInterface;

    constructor(payload: PickUpSuccessEventPayloadInterface) {
        this.payload = payload;
    }

    public getName(): string {
        return EventEnum.PICK_UP_SUCCESS;
    }

    public getPickedUpItem(): GameObject {
        return this.payload.pickedUpItem;
    }

    public getUpdatedInventory(): Array<GameObject> {
        return this.payload.updatedInventory;
    }

    public getCurrentLevel(): number {
        return this.payload.currentLevel;
    }
}