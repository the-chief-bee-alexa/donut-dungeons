import { EventEnum } from "../EventEnum";
import { InitialiseLevelFailReasonEnum } from "../../general_enums/InitialiseLevelFailReasonEnum";

export interface InitialiseLevelFailEventPayloadInterface {
    reason: InitialiseLevelFailReasonEnum;
}

export class InitialiseLevelFailEvent {
    private payload: InitialiseLevelFailEventPayloadInterface;

    constructor(payload: InitialiseLevelFailEventPayloadInterface) {
        this.payload = payload;
    }

    public getName(): string {
        return EventEnum.INITIALISE_LEVEL_FAIL;
    }

    public getReason(): InitialiseLevelFailReasonEnum {
        return this.payload.reason;
    }
}