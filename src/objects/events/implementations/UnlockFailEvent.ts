import { EventEnum } from "../EventEnum";
import { UnlockFailReasonEnum } from "../../general_enums/UnlockFailReasonEnum";

export interface UnlockFailEventPayloadInterface {
    reason: UnlockFailReasonEnum;
}

export class UnlockFailEvent {
    private payload: UnlockFailEventPayloadInterface;

    constructor(payload: UnlockFailEventPayloadInterface) {
        this.payload = payload;
    }

    public getName(): string {
        return EventEnum.UNLOCK_FAIL;
    }

    public getReason(): UnlockFailReasonEnum {
        return this.payload.reason;
    }
}