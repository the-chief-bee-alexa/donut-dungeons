import { EventEnum } from "../EventEnum";
import { Chest } from "../../items/Chest";
import { GameObject } from "../../GameObject";

export interface LevelCompletedOptionalData {
    openedChest?: Chest;
    lastDonut?: GameObject;
}

export interface LevelCompletedEventPayloadInterface {
    completedLevel: number;
    nextLevel: number;
    optionalData: LevelCompletedOptionalData;
}

export class LevelCompletedEvent {

    private payload: LevelCompletedEventPayloadInterface;

    constructor(payload: LevelCompletedEventPayloadInterface) {
        this.payload = payload;
    }

    public getName(): string {
        return EventEnum.LEVEL_COMPLETED;
    }

    public getCompletedLevel(): number {
        return this.payload.completedLevel;
    }

    public getNextLevel(): number {
        return this.payload.nextLevel;
    }

    public setOptionalData(optionalData: LevelCompletedOptionalData) {
        this.payload.optionalData = optionalData;
    }

    public getOptionalData(): LevelCompletedOptionalData {
        return this.payload.optionalData;
    }
}