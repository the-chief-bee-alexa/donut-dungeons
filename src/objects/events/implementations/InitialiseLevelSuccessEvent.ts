import { EventEnum } from "../EventEnum";

export class InitialiseLevelSuccessEvent {
    public getName(): string {
        return EventEnum.INITIALISE_LEVEL_SUCCESS;
    }
}