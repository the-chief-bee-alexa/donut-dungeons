import { GameObject } from "../GameObject";
import { GameObjectEnum } from "../GameObjectEnum";

export class Donut extends GameObject {

    private quantity: number;

    constructor(name: string, quantity: number) {
        super(GameObjectEnum.DONUT, name);

        this.quantity = quantity;
    }

    public setQuantity(quantity: number): void {
        this.quantity = quantity;
    }

    public getQuantity(): number {
        return this.quantity;
    }
}