import { GameObject } from "../GameObject";
import { GameObjectEnum } from "../GameObjectEnum";

export class Key extends GameObject {

    private keyId: number;

    constructor(keyId: number, name: string) {
        super(GameObjectEnum.KEY, name);
        this.keyId = keyId;
    }

    public setKeyId(keyId: number): void {
        this.keyId = keyId;
    }

    public getKeyId(): number {
        return this.keyId;
    }
}