import { GameObject } from "../GameObject";
import { GameObjectEnum } from "../GameObjectEnum";

export class Chest extends GameObject {

    private isLocked: boolean;
    private contains: Array<GameObject>;
    private openedWithKeyId: number;

    constructor(
        name: string,
        isLocked: boolean,
        contains: Array<GameObject>,
        openedWithKeyId: number
    ) {
        super(GameObjectEnum.CHEST, name);
        this.isLocked = isLocked;
        this.contains = contains;
        this.openedWithKeyId = openedWithKeyId;
    }

    public setIsLocked(isLocked: boolean): void {
        this.isLocked = isLocked;
    }

    public getIsLocked(): boolean {
        return this.isLocked;
    }

    public setContains(contains: Array<GameObject>): void {
        this.contains = contains;
    }

    public getContains(): Array<GameObject> {
        return this.contains;
    }

    public getItemByItemType(itemType: GameObjectEnum): GameObject | void {
        return this.contains.find((item: GameObject) => {
            return itemType === item.getType();
        });
    }

    public getDescription() {
        const chestName = this.getName();
        const contains: GameObject[] = this.getContains();

        if (this.getIsLocked()) {
            return `There is a locked ${chestName} here. It can be opened with a key.`;
        }

        if (contains.length >= 1) {
            return `There is an open ${chestName} here.` + this.getContainsDescription();
        }

        return `There is an open, empty ${chestName} here.`;
    }

    public getContainsDescription() {
        const contains: GameObject[] = this.getContains();

        if (contains.length >= 1) {
            let desc= `The chest contains `;
            contains.forEach((itemInChest: GameObject) => {
                const itemName = itemInChest.getName();
                desc = desc + `a ${itemName}`;
            });

            return desc + ".";
        }
    }

    public setOpenedWithKeyId(openedWithKeyId: number): void {
        this.openedWithKeyId = openedWithKeyId;
    }

    public getOpenedWithKeyId(): number {
        return this.openedWithKeyId;
    }
}