import { AbstractService } from "./AbstractService";
import { ServiceFactory } from "./ServiceFactory";
import { ServiceEnum } from "./ServiceEnum";
import { DatabaseProvider } from "../db/DatabaseProvider";

export class ServiceProvider {

    readonly dependencyTable = [
        {
            'serviceName': ServiceEnum.UserActionService,
            'dependencyNames': [
                ServiceEnum.UserService,
                ServiceEnum.CellService,
                ServiceEnum.DonutService
            ]
        }
    ];

    private dbProvider:  DatabaseProvider;
    private serviceFactory: ServiceFactory;
    private services: Array<AbstractService> = [];

    constructor(dbProvider: DatabaseProvider) {
        this.dbProvider = dbProvider;
        this.serviceFactory = new ServiceFactory(this.dbProvider);
    }

    public getService(serviceName: ServiceEnum): AbstractService {
        const service = this.serviceExists(serviceName);
        if (service) {
            return service;
        }

        const newService = this.createService(serviceName);
        this.services.push(newService);
        return newService;
    }

    private createService(serviceName: ServiceEnum): AbstractService {
        const dependencies = this.getDependencies(serviceName);
        return this.serviceFactory.create(serviceName, dependencies);
    }

    private serviceExists(serviceName: ServiceEnum): AbstractService | void {
        const service = this.services.find((element) => {
            return element.getName() === serviceName;
        });

        if (service) {
            return service;
        }
    }

    private getDependencies(serviceName: ServiceEnum): Array<AbstractService> {
        const depTable = this.dependencyTable.find((service) => {
            return service.serviceName === serviceName;
        });

        if (!depTable) {
            return [];
        }

        const res = depTable.dependencyNames.map((dependencyName) => {
            return this.getService(dependencyName);
        });

        return res;
    }
}