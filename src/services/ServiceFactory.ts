import { UserActionService } from "./implementations/UserActionService";
import { AbstractService } from "./AbstractService";
import { ServiceEnum } from "./ServiceEnum";
import { DatabaseProvider } from "../db/DatabaseProvider";
import { UserService } from "./implementations/UserService";
import { CellService } from "./implementations/CellService";
import { GameObjectFactory } from "../objects/GameObjectFactory";
import { DonutService } from "./implementations/DonutService";

export class ServiceFactory {
    private dbProvider: DatabaseProvider;

    constructor(dbProvider: DatabaseProvider) {
        this.dbProvider = dbProvider;
    }

    public create(serviceName: ServiceEnum, dependencies: Array<AbstractService>): AbstractService {
        switch (serviceName) {
            case ServiceEnum.UserActionService:
                return this.createUserActionService(dependencies);
            case ServiceEnum.UserService:
                return new UserService(this.dbProvider);
            case ServiceEnum.CellService:
                return new CellService(this.dbProvider);
            case ServiceEnum.DonutService:
                 return this.createDonutService();
        }

        throw new Error("Service not recognised.");
    }

    private getDependency(dependencyServiceName: ServiceEnum, dependencies: Array<AbstractService>): AbstractService {
        const dependencyService = dependencies.find((service) => {
            return service.getName() === dependencyServiceName;
        });

        if (dependencyService) {
            return dependencyService;
        }

        throw new Error("Could not inject the dependency service: " + dependencyServiceName);
    }

    private createUserActionService(dependencies: Array<AbstractService>): AbstractService {
        const userService = this.getDependency(ServiceEnum.UserService, dependencies) as UserService;
        const cellService = this.getDependency(ServiceEnum.CellService, dependencies) as CellService;
        const donutService = this.getDependency(ServiceEnum.DonutService, dependencies) as DonutService;

        return new UserActionService(
            this.dbProvider, 
            userService,
            cellService,
            donutService,
            new GameObjectFactory()
        );
    }

    private createDonutService(): AbstractService {
        return new DonutService(this.dbProvider, new GameObjectFactory());
    }

}