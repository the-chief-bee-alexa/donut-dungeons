export enum ServiceEnum {
    UserActionService = 'UserActionService',
    UserService = 'UserService',
    CellService = 'CellService',
    DonutService = 'DonutService'
}