import { ServiceEnum } from "./ServiceEnum";
import { DatabaseProvider } from "../db/DatabaseProvider";

export abstract class AbstractService {

    protected dbProvider: DatabaseProvider;

    constructor(dbProvider: DatabaseProvider) {
        this.dbProvider = dbProvider;
    }

    abstract getName(): ServiceEnum;
}