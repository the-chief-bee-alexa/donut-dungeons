import { AbstractService } from "../AbstractService";
import { DatabaseProvider } from "../../db/DatabaseProvider";
import { CellAccessor } from "../../db/accessors/CellAccessor";

import { ServiceEnum } from "../ServiceEnum";
import { AccessorEnum } from "../../db/AccessorEnum";
import { GameObjectEnum } from "../../objects/GameObjectEnum";
import { UnlockFailReasonEnum } from "../../objects/general_enums/UnlockFailReasonEnum";

import { GameObject } from "../../objects/GameObject";
import { Cell } from "../../objects/structures/Cell";
import { Chest } from "../../objects/items/Chest";
import { Key } from "../../objects/items/Key";
import { Door } from "../../objects/structures/Door";

import { UnlockFailEvent } from "../../objects/events/implementations/UnlockFailEvent";
import { UnlockSuccessEvent } from "../../objects/events/implementations/UnlockSuccessEvent";
import { Donut } from "../../objects/items/Donut";

export class CellService extends AbstractService {

    private cellAccessor: CellAccessor;

    constructor(dbProvider: DatabaseProvider) {
        super(dbProvider);

        this.cellAccessor = this.dbProvider.getAccessor(AccessorEnum.CellAccessor) as CellAccessor;
    }

    public getName(): ServiceEnum {
        return ServiceEnum.CellService;
    }

    public async getCell(userId: string, coords: string): Promise<Cell | void> {
        return this.cellAccessor.getCell(userId, coords);
    }

    public async pickUpItem(
        userId: string,
        cellCoords: string,
        updatedCellItems: Array<GameObject>,
        updatedInventoryItems: Array<GameObject>): Promise<Array<GameObject> | void> {
            return this.cellAccessor.pickUpItem(userId, cellCoords, updatedCellItems, updatedInventoryItems);
    }

    public async unlock(userId: string, cell: Cell, inventory: Array<GameObject>, itemType: GameObjectEnum): Promise<UnlockSuccessEvent | UnlockFailEvent> {
        switch (itemType) {
            case GameObjectEnum.CHEST:
                return this.unlockChest(userId, cell, inventory);
            case GameObjectEnum.DOOR:
                return this.unlockDoor(userId, cell, inventory);
            default:
                throw new Error("Item type not supported to be unlocked.");
        }
    }

    private async unlockChest(userId: string, cell: Cell, inventory: Array<GameObject>): Promise<UnlockSuccessEvent | UnlockFailEvent> {
        const res = cell.findItem(GameObjectEnum.CHEST);
        if (!res) return new UnlockFailEvent({reason: UnlockFailReasonEnum.NO_UNLOCKABLE_ITEM});

        const chest = res["0"] as Chest;
        const chestItemIndex = res["1"];

        if (!chest.getIsLocked()) return new UnlockFailEvent({reason: UnlockFailReasonEnum.ITEM_ALREADY_UNLOCKED});

        const key = this.findKey(inventory, chest.getOpenedWithKeyId());
        if (!key) return new UnlockFailEvent({reason: UnlockFailReasonEnum.NO_KEY});

        const contains = chest.getContains();

        const inventoryWithChestKeyRemoved = this.removeItem(inventory, GameObjectEnum.KEY);
        const newInventory = this.addItemsToInventory(contains, inventoryWithChestKeyRemoved);

        if (await this.cellAccessor.unlockChest(userId, cell.getCoords(), chestItemIndex, [], newInventory)) {
            return new UnlockSuccessEvent({
                unlockedItem: chest,
                updatedInventory: newInventory,
                optionalData: {}
            });
        }

        throw new Error("An error occurred when trying to unlock chest.");
    }

    private async unlockDoor(userId: string, cell: Cell, inventory: Array<GameObject>): Promise<UnlockSuccessEvent | UnlockFailEvent> {
        const res = cell.findStructure(GameObjectEnum.DOOR);
        if (!res) return new UnlockFailEvent({reason: UnlockFailReasonEnum.NO_UNLOCKABLE_ITEM});

        const door = res["0"] as Door;
        const doorDirection = res["1"];

        if (!door.getIsLocked()) return new UnlockFailEvent({reason: UnlockFailReasonEnum.ITEM_ALREADY_UNLOCKED});

        const key = this.findKey(inventory, door.getOpenedWithKeyId());
        if (!key) return new UnlockFailEvent({reason: UnlockFailReasonEnum.NO_KEY});

        const inventoryWithKeyRemoved = this.removeItem(inventory, GameObjectEnum.KEY);
        if (await this.cellAccessor.unlockDoor(userId, cell.getCoords(), doorDirection, inventoryWithKeyRemoved)) {
            return new UnlockSuccessEvent({
                unlockedItem: door,
                updatedInventory: inventoryWithKeyRemoved,
                optionalData: {}
            });
        }

        throw new Error("An error occurred when trying to unlock door.");
    }

    // TODO: There is a very similar copy of this method in UserActionService. Needs refactoring.
    private addItemsToInventory(pickedUpItems: GameObject[], inventory: Array<GameObject>): Array<GameObject> {
        pickedUpItems.forEach((pickedUpItem: GameObject) => {
            if (pickedUpItem.getType() === GameObjectEnum.DONUT) {
                const foundDonutType = inventory.find((item: GameObject) => {
                    return item.getName() === pickedUpItem.getName();
                });
                if (foundDonutType) {
                    this.updateDonutQuantity(pickedUpItem, foundDonutType);
                } else {
                    inventory.push(pickedUpItem);
                }
            } else {
                inventory.push(pickedUpItem);
            }    
        });

        return inventory;
    }

     // TODO: There is an exact copy of this method in UserActionService. Needs refactoring.
    private updateDonutQuantity(pickedUpItem: GameObject, foundDonutType: GameObject): void {
        const donutPickedUp = pickedUpItem as Donut;
        const donutInInventory = foundDonutType as Donut;
        donutInInventory.setQuantity(donutInInventory.getQuantity() + donutPickedUp.getQuantity());
    }

    private findKey(items: Array<GameObject>, keyId: number): Key | void {
        const key = items.find((item: GameObject) => {
            if (item.getType() === GameObjectEnum.KEY) {
                const key = item as Key;
                return key.getKeyId() === keyId;
            }
            return false;
        });

        return key as Key;
    }

    private removeItem(items: Array<GameObject>, itemTypeToRemove: GameObjectEnum) {
        return items.filter((item: GameObject) => {
            return item.getType() !== itemTypeToRemove;
        });
    }
}
