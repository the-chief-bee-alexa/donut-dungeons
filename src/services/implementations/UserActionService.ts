import { AbstractService } from "../AbstractService";
import { UserService } from "./UserService";
import { CellService } from "./CellService";
import { DonutService } from "./DonutService";
import { DatabaseProvider } from "../../db/DatabaseProvider";
import { UserAttributes } from "../../objects/general/UserAttributes";

import { ServiceEnum } from "../ServiceEnum";
import { DirectionEnum } from "../../objects/general_enums/DirectionEnum";
import { GameObjectEnum } from "../../objects/GameObjectEnum";
import { PickUpFailReasonEnum } from "../../objects/general_enums/PickUpFailReasonEnum";

import { Cell } from "../../objects/structures/Cell";
import { Chest } from "../../objects/items/Chest";
import { Donut } from "../../objects/items/Donut";
import { Corridor } from "../../objects/structures/Corridor";
import { Ladder } from "../../objects/structures/Ladder";
import { Door } from "../../objects/structures/Door";
import { GameObject } from "../../objects/GameObject";
import { GameObjectFactory } from "../../objects/GameObjectFactory";

import { LevelCompletedEvent, LevelCompletedOptionalData } from "../../objects/events/implementations/LevelCompletedEvent";
import { UnlockSuccessEvent } from "../../objects/events/implementations/UnlockSuccessEvent";
import { UnlockFailEvent } from "../../objects/events/implementations/UnlockFailEvent";
import { PickUpFailEvent } from "../../objects/events/implementations/PickUpFailEvent";
import { PickUpSuccessEvent } from "../../objects/events/implementations/PickUpSuccessEvent";

export type UnlockUserActionEvent = UnlockSuccessEvent | UnlockFailEvent | LevelCompletedEvent;

export class UserActionService extends AbstractService {

    private userService: UserService;
    private cellService: CellService;
    private donutService: DonutService;
    private gameObjectFactory: GameObjectFactory;

    constructor(
        dbProvider: DatabaseProvider,
        userService: UserService,
        cellService: CellService,
        donutService: DonutService,
        gameObjectFactory: GameObjectFactory
    ) {
        super(dbProvider);

        this.userService = userService;
        this.cellService = cellService;
        this.donutService = donutService;
        this.gameObjectFactory = gameObjectFactory;
    }

    public getName(): ServiceEnum {
        return ServiceEnum.UserActionService;
    }

    public async move(userId: string, direction: DirectionEnum): Promise<[boolean, Cell | void, GameObject]> {
        const userAtts: UserAttributes | void = await this.userService.getUserAttributes(userId, ["currentLocation"]);

        if (userAtts && userAtts.currentLocation) {
            const cell: Cell | void = await this.cellService.getCell(userId, userAtts.currentLocation);

            if (!cell) {
                throw new Error("Could not get Cell when trying to move.");
            }

            let nextCell;
            const gameObjectInDirection = cell.getGameObjectInDirection(direction);
            const moveIsValid = cell.isValidMove(direction);
            if (moveIsValid) {
                const newUserLocation = this.getNewUserLocation(cell, direction);
                await this.userService.setUserLocation(userId, newUserLocation);
                nextCell = await this.cellService.getCell(userId, newUserLocation);
            }

            return [moveIsValid, nextCell, gameObjectInDirection];
        }

        throw new Error("Could not get User attributes when trying to move.");
    }

    public async pickUpItem(userId: string, itemType: GameObjectEnum): Promise<PickUpSuccessEvent | PickUpFailEvent | LevelCompletedEvent> {
        if (itemType !== GameObjectEnum.KEY && itemType !== GameObjectEnum.DONUT) {
            throw new Error("Given item type not supported to be picked up.");
        }

        const userAtts: UserAttributes | void = await this.userService.getUserAttributes(
            userId,
            ["currentLocation", "inventory", "currentLevel"]
        );

        if (userAtts && userAtts.currentLocation && userAtts.inventory && userAtts.currentLevel) {
            const currentLevel = userAtts.currentLevel;
            const cell: Cell | void = await this.cellService.getCell(userId, userAtts.currentLocation);

            if (!cell) {
                throw new Error("Could not get Cell when trying to pick up item.");
            }

            const res = this.extractItemFromCell(cell, itemType);
            if (res) {
                const pickedUpItem = res["0"];
                const remainingCellItems = res["1"];

                const inventory = this.jsonInventoryToGameObjectArray(userAtts.inventory);
                const updatedInv = this.addItemToInventory(pickedUpItem, inventory);
                const updatedInventory = await this.cellService.pickUpItem(userId, cell.getCoords(), remainingCellItems, updatedInv);

                if (updatedInventory) {
                    if (itemType === GameObjectEnum.DONUT) {
                        const donutsToFind = this.donutService.calculateDonutsLeftToFind(currentLevel, updatedInventory);
                        if (donutsToFind.length === 0) {
                            return this.completeLevel(userId, currentLevel, {lastDonut: pickedUpItem});
                        }
                    }

                    return new PickUpSuccessEvent({pickedUpItem, updatedInventory, currentLevel});
                }
            }

            return new PickUpFailEvent({reason: PickUpFailReasonEnum.NO_SUCH_ITEM});
        }

        throw new Error("Was unable to retrieve user attributes from database when trying to pick up item.");
    }
    
    public async unlock(userId: string, itemType: GameObjectEnum): Promise<UnlockUserActionEvent> {
        if (itemType !== GameObjectEnum.CHEST && itemType !== GameObjectEnum.DOOR) {
            throw new Error("Given item type not supported to be unlock.");
        }

        const userAtts: UserAttributes | void = await this.userService.getUserAttributes(
            userId,
            ["currentLocation", "inventory", "currentLevel"]
        );

        if (userAtts && userAtts.currentLocation && userAtts.inventory && userAtts.currentLevel) {
            const cell: Cell | void = await this.cellService.getCell(userId, userAtts.currentLocation);
            if (!cell) {
                throw new Error("Could not get Cell when trying to unlock an item.");
            }

            const inventory = this.jsonInventoryToGameObjectArray(userAtts.inventory);

            const event = await this.cellService.unlock(userId, cell, inventory, itemType);
            if (event instanceof UnlockSuccessEvent) {
                event.setOptionalData({currentLevel: userAtts.currentLevel});
                const levelCompletedEvent = await this.completeLevelIfLastDonutInChest(userId, event);
                return levelCompletedEvent ? levelCompletedEvent : event;
            }

            return event;
        }

        throw new Error("An error occurred when trying to unlock chest.");
    }

    private async completeLevelIfLastDonutInChest(userId: string, event: UnlockSuccessEvent): Promise<LevelCompletedEvent | void> {
        const unlockedItem = event.getUnlockedItem();
        const inventory = event.getUpdatedInventory();
        
        if (unlockedItem instanceof Chest) {
            const donut = unlockedItem.getItemByItemType(GameObjectEnum.DONUT);
            if (donut) {
                const currentLevel = event.getOptionalData().currentLevel;
                if (!currentLevel) {
                    throw new Error("Current level must be specified to check if level has been completed.");
                }
    
                const donutsToFind = this.donutService.calculateDonutsLeftToFind(currentLevel, inventory);
                if (donutsToFind.length === 0) {
                    return this.completeLevel(userId, currentLevel, {openedChest: unlockedItem});
                }
            }
        }
    }

    private async completeLevel(userId: string, currentLevel: number, optionalData?: LevelCompletedOptionalData): Promise<LevelCompletedEvent> {
        if (await this.userService.completeLevel(userId, currentLevel)) {
            return new LevelCompletedEvent({
                completedLevel: currentLevel, 
                nextLevel: currentLevel + 1, 
                optionalData: optionalData ? optionalData : {}
            });
        }

        throw new Error("Was not able to complete gameState for user ID: " + userId);
    }

    private addItemToInventory(pickedUpItem: GameObject, inventory: Array<GameObject>): Array<GameObject> {
        if (pickedUpItem.getType() === GameObjectEnum.DONUT) {
            const foundDonutType = inventory.find((item: GameObject) => {
                return item.getName() === pickedUpItem.getName();
            });

            if (foundDonutType) {
                this.updateDonutQuantity(pickedUpItem, foundDonutType);
                return inventory;
            }
        }

        inventory.push(pickedUpItem);
        return inventory;
    }

    private updateDonutQuantity(pickedUpItem: GameObject, foundDonutType: GameObject): void {
        const donutPickedUp = pickedUpItem as Donut;
        const donutInInventory = foundDonutType as Donut;
        donutInInventory.setQuantity(donutInInventory.getQuantity() + donutPickedUp.getQuantity());
    }

    private extractItemFromCell(cell: Cell, itemType: GameObjectEnum): [GameObject, Array<GameObject>] | void {
        let foundItem = null;

        const existingCellItems = cell.getItems();
        const remainingCellItems = existingCellItems.filter((item: GameObject) => {
            if (item.getType() === itemType) {
                foundItem = item;
            }
            return item.getType() !== itemType;
        });

        if (foundItem) {
            return [foundItem, remainingCellItems];
        }
    }

    private jsonInventoryToGameObjectArray(jsonInventory: any): Array<GameObject> {
        return jsonInventory.map((itemJson: any) => {
            return this.gameObjectFactory.createItemFromJson(itemJson);

        });
    }

    private getNewUserLocation(cell: Cell, direction: DirectionEnum): string {
        const objInDirection = cell.getGameObjectInDirection(direction);
        if (objInDirection instanceof Corridor || objInDirection instanceof Ladder || objInDirection instanceof Door) {
            return objInDirection.getLeadsTo();
        }

        throw new Error("Was not able to retrieve new user location to move to.");
    }
}
