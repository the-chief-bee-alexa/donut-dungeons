import { services } from "ask-sdk-model";

import { AbstractService } from "../AbstractService";
import { ServiceEnum } from "../ServiceEnum";
import { DatabaseProvider } from "../../db/DatabaseProvider";

import { AccessorEnum } from "../../db/AccessorEnum";
import { GameStateEnum } from "../../objects/general_enums/GameStateEnum";
import { UserAccessor } from "../../db/accessors/UserAccessor";

import { User } from "../../objects/general/User";
import { UserAttributes } from "../../objects/general/UserAttributes";
import { levelSelector } from "../../levels/levelSelector";
import { PREMIUM_LEVEL_STARTER_LEVEL, PREMIUM_LEVELS_REF_NAME } from "../../handlers/in_skill_purchasing/WhatCanIBuyIntentHandler";
import { InitialiseLevelFailEvent } from "../../objects/events/implementations/InitialiseLevelFailEvent";
import { InitialiseLevelSuccessEvent } from "../../objects/events/implementations/InitialiseLevelSuccessEvent";
import { InitialiseLevelFailReasonEnum } from "../../objects/general_enums/InitialiseLevelFailReasonEnum";

export type InitialiseLevelEvent = InitialiseLevelSuccessEvent | InitialiseLevelFailEvent;

export class UserService extends AbstractService {

    private userAccessor: UserAccessor;

    constructor(dbProvider: DatabaseProvider) {
        super(dbProvider);
        this.userAccessor = this.dbProvider.getAccessor(AccessorEnum.UserAccessor) as UserAccessor;
    }

    public getName(): ServiceEnum {
        return ServiceEnum.UserService;
    }

    public async getUserById(userId: string): Promise<User | void> {
        try {
            return this.userAccessor.getUserById(userId);
        } catch (exception) {
            return;
        }
    }

    public async createUser(userId: string): Promise<User | void> {
        const now = new Date().getTime();
        const levelData =  levelSelector(1);
        const worldState = levelData["0"];
        const startLocation = levelData["1"];
        
        const user = new User(
            userId,
            1,
            startLocation,
            worldState,
            [],
            now,
            now,
            1,
            GameStateEnum.IN_PROGRESS,
            []
        );

        try {
            await this.userAccessor.addUser(user);
            return user;
        } catch (exception) {
            return;
        }
    }

    public async recordLogin(userId: string, lastLoginDate: number): Promise<boolean> {
        return this.userAccessor.recordLogin(userId, lastLoginDate);
    }

    public async setUserLocation(userId: string, newValue: string): Promise<string | void> {
        return this.userAccessor.setUserLocation(userId, newValue);
    }

    public async getUserAttributes(userId: string, attributes: Array<string>): Promise<UserAttributes | void> {
        return this.userAccessor.getUserAttributes(userId, attributes);
    }

    public async completeLevel(userId: string, completedLevel: number): Promise<boolean> {
        return this.userAccessor.completeLevel(userId, completedLevel);
    }

    public async initialiseLevel(
        userId: string,
        initialisedLevel: number,
        serviceClientFactory: services.ServiceClientFactory | undefined,
        locale: string | undefined
    ): Promise<InitialiseLevelEvent> {
        const userAtts: UserAttributes | void = await this.getUserAttributes(userId, ["completedLevels"]);

        if (!userAtts || !userAtts.completedLevels) {
            throw new Error("Was not able to retrieve user information to initialise level.");
        }

        const highestCompleteLevel = userAtts.completedLevels.reduce((a, b) => Math.max(a, b));
        if (initialisedLevel > highestCompleteLevel + 1) {
            return new InitialiseLevelFailEvent({reason: InitialiseLevelFailReasonEnum.LEVEL_NOT_REACHED});
        }

        const entitled = await this.userEntitledtoLevel(initialisedLevel, serviceClientFactory, locale);
        if (!entitled) {
            return new InitialiseLevelFailEvent({reason: InitialiseLevelFailReasonEnum.USER_NOT_ENTITLED});
        }

        const levelInitialised = await this.userAccessor.initialiseLevel(userId, initialisedLevel);
        if (!levelInitialised) {
            throw new Error("Was unable to initialise level in database.");
        }

        return new InitialiseLevelSuccessEvent();
    }

    private async userEntitledtoLevel(
        level: number,  
        serviceClientFactory: services.ServiceClientFactory | undefined,
        locale: string | undefined
    ): Promise<boolean> {
        if (level < PREMIUM_LEVEL_STARTER_LEVEL) {
            return true;
        }

        if (serviceClientFactory && locale) {
            const ms = serviceClientFactory.getMonetizationServiceClient();
            const productsResponse = await ms.getInSkillProducts(locale);

            const premiumLevels = productsResponse.inSkillProducts.filter(
                record => record.referenceName === PREMIUM_LEVELS_REF_NAME
            );

            if (!premiumLevels) {
                throw new Error("Unable to retrieve data from the In skill Products endpoint");
            }

            return premiumLevels[0].entitled === 'ENTITLED';
        }

        return false;
    }
}