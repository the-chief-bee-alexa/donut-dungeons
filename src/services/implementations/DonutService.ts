import { AbstractService } from "../AbstractService";
import { ServiceEnum } from "../ServiceEnum";
import { DatabaseProvider } from "../../db/DatabaseProvider";

import { GameObjectFactory } from "../../objects/GameObjectFactory";
import { GameObject } from "../../objects/GameObject";
import { Donut } from "../../objects/items/Donut";
import donutLevelTable from "../../levels/donuts.json";
import { GameObjectEnum } from "../../objects/GameObjectEnum";

export class DonutService extends AbstractService {

    private gameObjectFactory: GameObjectFactory;

    constructor(dbProvider: DatabaseProvider, gameObjectFactory: GameObjectFactory) {
        super(dbProvider);

        this.gameObjectFactory = gameObjectFactory;
    }

    public getName(): ServiceEnum {
        return ServiceEnum.DonutService;
    }

    public calculateDonutsLeftToFind(currentLevel: number, inventory: Array<GameObject>):  Array<GameObject> {
        const donutsToFindTable = donutLevelTable.find((info: any) => {
            return info.level === currentLevel;
        });

        if (!donutsToFindTable) {
            throw new Error("Was unable to retrieve donut information for level.");
        }

        let donutsLeft: Array<GameObject> = [];
        donutsToFindTable.donuts.forEach((donut: any) => {
            const found = inventory.find((item: GameObject) => {
                return item instanceof Donut && item.getName() === donut.name;
            });

            if (!found) {
                const donutJson = {
                    "type": GameObjectEnum.DONUT,
                    "name": donut.name,
                    "quantity": donut.quantity
                }
                const donutObj = this.gameObjectFactory.createItemFromJson(donutJson);
                donutsLeft.push(donutObj);
            } else {
                const foundDonut = found as Donut;
                if (foundDonut.getQuantity() !== donut.quantity) {
                    const donutJson = {
                        "type": GameObjectEnum.DONUT,
                        "name": donut.name,
                        "quantity": donut.quantity - foundDonut.getQuantity()
                    }

                    const donutObj = this.gameObjectFactory.createItemFromJson(donutJson);
                    donutsLeft.push(donutObj);
                }
            }
        });

        return donutsLeft;
    }
}